#!/usr/bin/env python3
import argparse
import os
import io
import ast
import re
import base64
from pathlib import Path
from fastecdsa import keys, curve, ecdsa
from fastecdsa.encoding.pem import PEMEncoder
from pyrogram.raw.core.primitives import Bytes

REGEX = re.compile(r'(__signature__\s+=\s+["\'])(?P<sig>SHSIG-[A-Za-z0-9+/=]+)(["\'])')

def _encode_int(i: int) -> bytes:
	return i.to_bytes(32, "big")

def SignFile(path: Path, d: int):
	with open(path, "r+") as pyfile:
		contents = pyfile.read()
		tree = ast.parse(contents)
		has_signature = False
		for node in tree.body:
			if isinstance(node, ast.Assign) and isinstance(node.value, ast.Constant):
				for target in node.targets:
					if target.id.lower() == "__signature__":
						has_signature = True
						tree.body.remove(node)
		data = ast.unparse(tree)
		r, s = ecdsa.sign(data, d)
		rs = Bytes(_encode_int(r)) + Bytes(_encode_int(s))
		signature = "SHSIG-" + base64.b64encode(rs).decode()
		if has_signature:
			matches = REGEX.search(contents)
			if matches.group("sig") != signature:
				print(f"Updating signature for {path}: {signature}")
				contents = REGEX.sub(fr'\1{signature}\3', contents)
				pyfile.seek(0, io.SEEK_SET)
				pyfile.write(contents)
		else:
			print(f"Signing {path}: {signature}")
			pyfile.seek(0, io.SEEK_END)
			pyfile.write(f"\n\n__signature__ = \"{signature}\"")

def main() -> int:
	parser = argparse.ArgumentParser(description="Cryptographically sign ShadowHawk plugins", epilog="NOTE: Plugins MUST be resigned when any python code changes!")
	parser.add_argument('-k', '--privkey', type=Path, help="Path to private key file")
	parser.add_argument('-p', '--plugin', type=Path, help="Path to plugin file or directory")
	parser.add_argument('-g', '--generate', help="Generate a new private key for signing", action='store_true')
	parser.add_argument('--pubkey', help="Print the public key from from the private key", action='store_true')
	args = parser.parse_args()

	if not args.privkey:
		print("A private key location must be specified!")
		return 1
	if args.privkey.exists():
		if not args.privkey.is_file():
			print("Private key must be a file!")
			return 1
	else:
		if not args.generate:
			print("You must generate a private key!")
			return 1

	if args.generate:
		d, Q = keys.gen_keypair(curve.P256)
		with open(args.privkey, "w") as privfile:
			privfile.write(PEMEncoder.encode_private_key(d, curve=curve.P256))
		
		print(f"OwO Generated new keypair at {args.privkey}!\n")
		print(PEMEncoder.encode_public_key(Q))
		return 0
	
	d, Q = keys.import_key(args.privkey)
	if args.pubkey:
		print(PEMEncoder.encode_public_key(Q))
		print(f"\nX: {Q.x}\nY: {Q.y}")
		return 0

	if args.plugin.is_dir():
		for path in sorted(args.plugin.rglob("*.py")):
			SignFile(path, d)
	elif args.plugin.is_file():
		SignFile(args.plugin, d)
	else:
		print(f"Error: Unknown file: {args.plugin}")
		return 1
		
	return 0


if __name__ == "__main__":
	os._exit(main())