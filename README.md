![](assets/shadowhawk.png)

# ShadowHawk

An overseeing userbot that has multiple accounts and a slave attached

## Features

 - Advanced logging of many events on telegram
 - Multi-account capable
 - Query most anti-spam organizations on telegram
 - Advanced administrative tools for group moderation
 - Useful commands to query wikipedia, urban dictionary, google translate, or convert from metric to imperial
 - Secure and verifiable plugin system
 - Userbot is totally silent until requested upon (no random commands or events)
 - No nonsense. No "meme" commands like ".cat" or ".bigspam"

### Installation Instructions
1. Install:
    - `python3` (this is in python after all)
    - `postgresql` (or similar async-capable database)
    - `bind9` for the `.dig` userbot command
2. `pip3 install -r requirements.txt`
3. Copy example-config.yaml to config.yaml and edit it
4. `mkdir sessions`


### Start
`python3 -m shadowhawk`  
After that, send .help somewhere and have fun :D

### Extras
There are extra modules more specific to me that may interest developers and advanced users. They can be found [here](https://gitlab.com/Justasic/shadowhawk-extras).

### Bugs/Support/Whatever
- Join the [support chat](https://t.me/ShadowHawkSupport).
- Subscribe to the [news channel](https://t.me/ShadowHawkNews).

## Frequently Asked Questions

**Q:** _Will this work on Heroku (or similar platforms)?_ \
**A:** I don't directly support Heroku nor will I provide support for Heroku-related errors. ShadowHawk heavily relies upon the database and is required for it's operation. I have heard some users get ShadowHawk working on Heroku using something like ElephantSQL(?) but you would have to ask them.

**Q:** _Why do you sign plugins?_ \
**A:** One of my fears when first learning about userbots was account hijacking, whether by the userbot software itself or a 3rd party plugin. While you have to trust that ShadowHawk won't do something nefarious (it never will), I have seen other people load custom plugins into their userbot and it session-jacks them. Spammers use this to mass-add or send tons of spam to different chats and then gets you limited by [@SpamBot](https://t.me/SpamBot) and/or you end up in an anti-spam system like SpamProtectionBot or SpamWatch. ShadowHawk attempts to stop this by cryptographically verifying the plugin's authenticity before loading it. When plugin verification is enforced, session-jacking plugins cannot be loaded thus thwarting the account hijcking attempt.

**Q:** _I wrote a custom plugin, how do I get it verified?_ \
**A:** At this time, signed plugins are only available within ShadowHawk's existing plugins. I will be implementing a "Plugin Store" similar to app stores on mobile platforms. Plugins will be verified and signed by ShadowHawk developers and available both as website and within ShadowHawk itself. Users can ensure that all plugins from the store are safe and won't do anything nefarious. In the meantime, if you need custom plugins then please disable plugin verification until the store is available.

**Q:** _I can't develop plugins because they're not signed, how do I create plugins?_ \
**A:** You must disable plugin verification during development. The only negative effects this has is that you lose session-jacking protection and the support chat won't accept bug reports in certain situations. Once your plugin is finished and the plugin store is available, you can upload it
to the developer portal on the website and publish it. After a review from one of the ShadowHawk developers, it will be published for everyone to use.

**Q:** _Why won't the support chat accept bug reports when plugin verification is disabled?_ \
**A:** Plugin verification also ensures that the plugin was not modified. If the bug occurs on a signed plugin then this bug can be investigated and fixed, however if plugin verification is disabled or the plugin fails verification then we cannot ensure that the bug was not a result of the user modifying the code.

**Q:** _Are you a furry?_ \
**A:** ***awoo*** 🐾

## Special Thanks
 - [Kneesocks](https://gitlab.com/blankX/): [Original implementation](https://gitlab.com/blankX/sukuinote) and help along the way
 - [Dank-del](https://gitlab.com/Dank-del): For giving me ideas and helping out
 - [Nocturn9x](https://github.com/nocturn9x): For helping with the design of plugin signing and building the [asyncevents](https://github.com/nocturn9x/asyncevents) library used heavily throughout the codebase.