#!/usr/bin/env python3
import asyncio
import ormar
import databases
import ziproto
import typing
import enum
import sys
import threading
import uuid as uniqueid
from datetime import datetime
from sqlalchemy import create_engine, MetaData
from concurrent.futures import ThreadPoolExecutor
from tqdm import tqdm

# Dirty hack to get around python's global variable scope copying
# bulllshit. Thanks @TheKneesocks for this code!
VT = typing.TypeVar('VT')
class ObjectProxy(typing.Generic[VT]):
	def __init__(self, thing: typing.Optional[VT] = None) -> None:
		self.thing: typing.Optional[VT] = thing

	def __getattr__(self, attr: str) -> typing.Any:
		if self.thing:
			return getattr(self.thing, attr)
		else:
			raise 

	def __bool__(self) -> bool:
		return bool(self.thing)

	def __repr__(self) -> str:
		return repr(self.thing)

	def __str__(self) -> str:
		return str(self.thing)

	def __iter__(self) -> typing.Iterator[typing.Any]:
		if isinstance(self.thing, typing.Iterable):
			yield iter(self.thing)
		else:
			raise RuntimeError(f"{repr(self.thing)} is not iterable")

	def __call__(self, *args: typing.List[typing.Any], **kwargs: typing.Dict[typing.Any, typing.Any]) -> typing.Any:
		if callable(self.thing):
			return self.thing(*args, **kwargs)
		else:
			raise RuntimeError(f"{repr(self.thing)} is not callable")

	def get_thing(self) -> typing.Optional[VT]:
		return self.thing

	def set_thing(self, thing: typing.Optional[VT]) -> None:
		self.thing = thing

PAGESZ = 1000

class UserBlob(enum.IntEnum):
	USER_ID = 0
	USER_FIRSTNAME = 1
	USER_LASTNAME = 2
	USER_CREATION = 3
	USER_UPDATE = 4
	USER_USERNAME = 5

class ChatBlob(enum.IntEnum):
	CHAT_ID = 0
	CHAT_ID_SHORT = 1
	CHAT_USERNAME = 2
	CHAT_TITLE = 3
	CHAT_CREATION = 4
	CHAT_UPDATED = 5
	CHAT_ADDEDCNT = 6

class AdderBlob(enum.IntEnum):
	ADDER_ID = 0
	ADDER_FIRSTNAME = 1
	ADDER_LASTNAME = 2
	ADDER_USERNAME = 3
	ADDER_CREATION = 4
	ADDER_UPDATED = 5
	ADDER_ADDED = 6
	ADDER_EVENTS = 7
	ADDER_SEENACT = 8

class EventBlob(enum.IntEnum):
	EVENT_UUID = 0
	EVENT_CHATID = 1
	EVENT_APS = 2
	EVENT_BEGIN = 3
	EVENT_END = 4
	EVENT_PARTICIPANTS = 5
	EVENT_LOGMSG = 6

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print(f"{sys.argv[0]} <from db URI> <to db URI>")
		sys.exit(1)

# Start up ormar
databaseOld = databases.Database(sys.argv[1], min_size=1, max_size=50)
databaseNew = databases.Database(sys.argv[2], min_size=1, max_size=50)
metadataOld = MetaData()
metadataNew = MetaData()

# Ormar base model
class BaseModelOld(ormar.ModelMeta):
	# Set the Ormar stuff
	metadata = metadataOld
	database = databaseOld

class BaseModelNew(ormar.ModelMeta):
	# Set the Ormar stuff
	metadata = metadataNew
	database = databaseNew

class MassAddEventOld(ormar.Model):
	class Meta(BaseModelOld):
		tablename = 'MassAddEvent'

	uuid: str = ormar.String(max_length=36, primary_key=True)
	blob: bytes = ormar.LargeBinary(max_length=1073742000)

class UserOld(ormar.Model):
	class Meta(BaseModelOld):
		tablename = 'User'

	id: int = ormar.BigInteger(primary_key=True)
	blob: bytes = ormar.LargeBinary(max_length=1073742000)

class AdderOld(ormar.Model):
	class Meta(BaseModelOld):
		tablename = 'MassAdder'

	id: int = ormar.BigInteger(primary_key=True)
	blob: bytes = ormar.LargeBinary(max_length=1073742000)

class ChatroomOld(ormar.Model):
	class Meta(BaseModelOld):
		tablename = 'Chatroom'

	id: int = ormar.BigInteger(primary_key=True)
	blob: bytes = ormar.LargeBinary(max_length=1073742000)

##########################################################################

class MAUser(ormar.Model):
	class Meta(BaseModelNew):
		pass

	id: int = ormar.BigInteger(primary_key=True)
	first_name: typing.Optional[str] = ormar.String(max_length=64, nullable=True)
	last_name: typing.Optional[str] = ormar.String(max_length=64, nullable=True)
	username: typing.Optional[str] = ormar.String(max_length=32, nullable=True)
	record_creation: datetime = ormar.DateTime(default=datetime.now())
	record_updated: datetime = ormar.DateTime(default=datetime.now())

	# Account that saw this user will be stored in metadata.
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAChatroom(ormar.Model):
	class Meta(BaseModelNew):
		pass

	id: int = ormar.BigInteger(primary_key=True)
	username: typing.Optional[str] = ormar.String(max_length=32, nullable=True)
	title: str = ormar.String(max_length=256)
	record_creation: datetime = ormar.DateTime(default=datetime.now())
	record_updated: datetime = ormar.DateTime(default=datetime.now())
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAEvent(ormar.Model):
	class Meta(BaseModelNew):
		pass

	uuid: uniqueid.UUID = ormar.UUID(primary_key=True)
	chat: MAChatroom = ormar.ForeignKey(MAChatroom)
	aps: float = ormar.Float()
	begin_time: datetime = ormar.DateTime(default=datetime.now())
	end_time: datetime = ormar.DateTime(default=datetime.now())
	# participants: typing.List['MAAddRecord'] = ormar.ManyToMany('MAAddRecord', virtual=True, related_name="eventparticipants")

	# Log message location will be kept in metadata blob.
	metadata: typing.Optional[bytes] = ormar.LargeBinary(max_length=1073742000, nullable=True)

class MAAddRecord(ormar.Model):
	class Meta(BaseModelNew):
		pass

	int: id = ormar.BigInteger(primary_key=True, autoincrement=True)
	creation: typing.Optional[datetime] = ormar.DateTime(default=datetime.now(), nullable=True)
	adder: MAUser = ormar.ForeignKey(MAUser, related_name="maadder")
	added: MAUser = ormar.ForeignKey(MAUser, related_name="maadded")
	event: MAEvent = ormar.ForeignKey(MAEvent)

# Open a new SQL connection to the database
ormar_sqlengine = create_engine(sys.argv[2])
# Create the relations
metadataNew.drop_all(ormar_sqlengine)
metadataNew.create_all(ormar_sqlengine)

executor = ThreadPoolExecutor(40)

async def WaitForFutures(futures):
	while True:
		found = False
		for future in futures:
			if not future.done():
				found = True
			else:
				future.result()
		if not found:
			break
		await asyncio.sleep(1)

async def migrateusers():

	lock = threading.Lock()
	bar = tqdm(desc="Migrating Users", unit="record")

	def _submitchunk(page: int, pages: int, ev):
		objs = asyncio.run_coroutine_threadsafe(UserOld.objects.paginate(page, PAGESZ).all(), ev).result()
		# print(f"Getting page {page} of size {PAGESZ}: {page*PAGESZ}")
		inserts = []
		for o in objs:
			user = ziproto.decode(o.blob)
			inserts.append(MAUser(id=int(o.id),
						first_name=str(user[UserBlob.USER_FIRSTNAME]) if UserBlob.USER_FIRSTNAME in user else None,
						last_name=str(user[UserBlob.USER_LASTNAME]) if UserBlob.USER_LASTNAME in user else None,
						username=str(user[UserBlob.USER_USERNAME]) if UserBlob.USER_USERNAME in user else None,
						record_creation=datetime.fromtimestamp(user[UserBlob.USER_CREATION]),
						record_updated=datetime.fromtimestamp(user[UserBlob.USER_UPDATE])
			))
		asyncio.run_coroutine_threadsafe(MAUser.objects.bulk_create(inserts), ev)
		with lock:
			bar.update(len(objs))

	size = await UserOld.objects.count()
	pages = round(size / PAGESZ + (1 if size % PAGESZ > 0 else 0))
	bar.total = size
	# print(f"Total of {size} users to migrate, beginning migration of {pages} user pages, page size is {PAGESZ}")
	await WaitForFutures([executor.submit(_submitchunk, page, pages, loop) for page in range(1, pages)])

async def migratechats():
	lock = threading.Lock()
	bar = tqdm(desc="Migrating Chats", unit="record")

	def _submitchunk(page, pages, ev):
		objs = asyncio.run_coroutine_threadsafe(ChatroomOld.objects.paginate(page, PAGESZ).all(), ev).result()
		inserts = []
		for o in objs:
			chat = ziproto.decode(o.blob)
			# Keep some basic statistics I guess in the metadata
			metadata = {
				0: chat[ChatBlob.CHAT_ADDEDCNT] # number of users added to the chat
			}
			ch_id = o.id
			if not ch_id < 0:
				ch_id = int(f"-100{ch_id}")
			inserts.append(MAChatroom(id=ch_id,
							username=str(chat[ChatBlob.CHAT_USERNAME]) if ChatBlob.CHAT_USERNAME in chat else None,
							title=str(chat[ChatBlob.CHAT_TITLE]),
							record_creation=datetime.fromtimestamp(chat[ChatBlob.CHAT_CREATION]),
							record_updated=datetime.fromtimestamp(chat[ChatBlob.CHAT_UPDATED]),
							metadata=bytes(ziproto.encode(metadata))
			))
		asyncio.run_coroutine_threadsafe(MAChatroom.objects.bulk_create(inserts), ev)
		with lock:
			bar.update(len(objs))
	
	size = await ChatroomOld.objects.count()
	pages = round(size / PAGESZ + (1 if size % PAGESZ > 0 else 0))
	bar.total = size
	await WaitForFutures([executor.submit(_submitchunk, page, pages, loop) for page in range(1, pages+1)])

async def migrateadders():
	lock = threading.Lock()
	bar = tqdm(desc="Migrating Adders", unit="record")

	def _submitchunk(page, pages, ev):
		objs = asyncio.run_coroutine_threadsafe(AdderOld.objects.paginate(page, PAGESZ).all(), ev).result()
		inserts = []
		for o in objs:
			uo = asyncio.run_coroutine_threadsafe(MAUser.objects.get_or_none(id=int(o.id)), ev).result()
			user = ziproto.decode(o.blob)

			if not uo:
				metadata = None
				if AdderBlob.ADDER_SEENACT in user:
					metadata = bytes(ziproto.encode({
						0: user[AdderBlob.ADDER_SEENACT]  # Which account saw this user.
					}))
				try:
					inserts.append(MAUser(id=int(o.id),
								first_name=str(user[AdderBlob.ADDER_FIRSTNAME]) if AdderBlob.ADDER_FIRSTNAME in user else None,
								last_name=str(user[AdderBlob.ADDER_LASTNAME]) if AdderBlob.ADDER_LASTNAME in user else None,
								username=str(user[AdderBlob.ADDER_USERNAME]) if AdderBlob.ADDER_USERNAME in user else None,
								record_creation=datetime.fromtimestamp(user[AdderBlob.ADDER_CREATION]),
								record_updated=datetime.fromtimestamp(user[AdderBlob.ADDER_UPDATED]),
								metadata=metadata
					))
				except Exception as ex:
					print("Exception processing:", user, metadata)
					raise ex
		if inserts:
			asyncio.run_coroutine_threadsafe(MAUser.objects.bulk_create(inserts), ev).result()
		with lock:
			bar.update(len(objs))
	
	size = await AdderOld.objects.count()
	pages = round(size / PAGESZ + (1 if size % PAGESZ > 0 else 0))
	bar.size = size
	await WaitForFutures([executor.submit(_submitchunk, page, pages, loop) for page in range(1, pages+1)])

async def migrateevents():
	lock = threading.Lock()
	bar = tqdm(desc="Migrating Events", unit="record")

	def _submitchunk(page, pages, ev):
		objs = asyncio.run_coroutine_threadsafe(MassAddEventOld.objects.paginate(page, PAGESZ).all(), ev).result()
		# print(f"returned {len(objs)} objects for page {page}")
		for o in objs:
			event = ziproto.decode(o.blob)

			metadata = None
			if EventBlob.EVENT_LOGMSG in event:
				metadata = bytes(ziproto.encode({
					0: event[EventBlob.EVENT_LOGMSG]
				}))
			
			chat = asyncio.run_coroutine_threadsafe(MAChatroom.objects.get(id=int(f"-100{event[EventBlob.EVENT_CHATID]}")), ev).result()

			e = MAEvent(uuid=uniqueid.UUID(o.uuid),
								chat=chat,
								aps=event[EventBlob.EVENT_APS],
								begin_time=datetime.fromtimestamp(event[EventBlob.EVENT_BEGIN]),
								end_time=datetime.fromtimestamp(event[EventBlob.EVENT_END]),
								metadata=metadata,
								# participants=users
			)
			# this has to be saved now due to needing to insert more records below
			asyncio.run_coroutine_threadsafe(e.save(), ev).result()

			# First collect all the users and their added counts.
			userids = []
			for pair in event[EventBlob.EVENT_PARTICIPANTS]:
				user_id, cnt = pair
				if not user_id in userids:
					userids.append(user_id)

			# query for the users from our new users table
			users = asyncio.run_coroutine_threadsafe(AdderOld.objects.filter(id__in=userids).all(), ev).result()
			# print(f"{len(users)} users returned")

			# now shit gets complicated, query for every user in the users
			# list and then get their added users
			insertions = []
			for user in users:
				adder_array = ziproto.decode(user.blob)
				adder = asyncio.run_coroutine_threadsafe(MAUser.objects.get(id=user.id), ev).result()
				# Start by getting all the chats we need to get from the database.
				for chat_id, user_id in adder_array[AdderBlob.ADDER_ADDED]:
					if chat.id == chat_id:
						mauser = asyncio.run_coroutine_threadsafe(MAUser.objects.get(id=user_id), ev).result()
						insertions.append(MAAddRecord(adder=adder, added=mauser, event=e))
			asyncio.run_coroutine_threadsafe(MAAddRecord.objects.bulk_create(insertions), ev).result()
		with lock:
			bar.update(len(objs))
	
	size = await MassAddEventOld.objects.count()
	pages = round(size / PAGESZ + (1 if size % PAGESZ > 0 else 0))
	bar.total = size
	await WaitForFutures([executor.submit(_submitchunk, page, pages, loop) for page in range(1, pages+1)])

async def main(loop) -> int:
	if not databaseOld.is_connected:
		await databaseOld.connect()
	if not databaseNew.is_connected:
		await databaseNew.connect()

	print("Collecting all user objects")
	await migrateusers()

	print(" "*150, end="\r")
	print("`User' table migration complete!")

	await migratechats()

	print(" "*150, end="\r")
	print("`Chatroom' migration complete!")

	await migrateadders()

	print(" "*150, end="\r")
	print("`Adder' table migration complete!")

	# Now the hardest part: migrating the MassAdd events
	print("Collecting all massadd objects")
	
	await migrateevents()

	print(" "*150, end="\r")
	print("Migration complete!")

	if databaseNew.is_connected:
		await databaseNew.disconnect()
	if databaseOld.is_connected:
		await databaseOld.disconnect()
	
	return 0

if __name__ == "__main__":
	loop = asyncio.new_event_loop()
	asyncio.set_event_loop(loop)
	sys.exit(loop.run_until_complete(main(loop)))
	# sys.exit(main())