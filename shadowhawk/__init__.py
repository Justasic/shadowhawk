
import asyncio
import yaml
import aiohttp
import logging
import typing
from rich.logging import RichHandler
from pathlib import Path

from pyrogram.parser import parser

__version__ = "1.0.0"

logging.basicConfig(
	format="[%(asctime)s] In thread %(threadName)s, module %(module)s at %(funcName)s line %(lineno)s -> %(message)s",
	level=logging.INFO,
	handlers=[RichHandler()],
)

# Dirty hack to get around python's global variable scope copying
# bulllshit. Thanks @TheKneesocks for this code!
VT = typing.TypeVar('VT')
class ObjectProxy(typing.Generic[VT]):
	def __init__(self, thing: typing.Optional[VT] = None) -> None:
		self.thing: typing.Optional[VT] = thing

	def __getattr__(self, attr: str) -> typing.Any:
		if self.thing:
			return getattr(self.thing, attr)
		else:
			raise 

	def __bool__(self) -> bool:
		return bool(self.thing)

	def __repr__(self) -> str:
		return repr(self.thing)

	def __str__(self) -> str:
		return str(self.thing)

	def __iter__(self) -> typing.Iterator[typing.Any]:
		if isinstance(self.thing, typing.Iterable):
			yield iter(self.thing)
		else:
			raise RuntimeError(f"{repr(self.thing)} is not iterable")

	def __call__(self, *args: typing.List[typing.Any], **kwargs: typing.Dict[typing.Any, typing.Any]) -> typing.Any:
		if callable(self.thing):
			return self.thing(*args, **kwargs)
		else:
			raise RuntimeError(f"{repr(self.thing)} is not callable")

	def get_thing(self) -> typing.Optional[VT]:
		return self.thing

	def set_thing(self, thing: typing.Optional[VT]) -> None:
		self.thing = thing

with open("config.yaml") as c:
	config = yaml.safe_load(c)

# Globals.
loop = asyncio.get_event_loop()
apps = dict()
app_user_ids = dict()
log_peer_ids = []
statistics = dict()
slave = ObjectProxy()
plmgr = ObjectProxy()

# Get the info on what the server supports
server_support = ObjectProxy()

# Http client session for making various HTTP requests
session = aiohttp.ClientSession()

# awaited task load measurements
loads = {
	1: 0.0,  # 1 minute load
	5: 0.0,  # 5 minute load
	15: 0.0,  # 15 minute load
	30: 0.0,  # 30 minute load
}

# this code here exists because i can't be fucked
class Parser(parser.Parser):
	async def parse(self, text, mode):
		if mode == "through":
			return text
		return await super().parse(text, mode)

#################
# ShadowHawk main
#

sessions_path = Path("sessions")
sessions_test_path = sessions_path / "test"
sessions_path.mkdir(exist_ok=True)

# This is here to avoid circular includes.
from shadowhawk.utils.AdminCacheManager import AdminCacheManager
acm = AdminCacheManager()

###############
