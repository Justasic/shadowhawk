import asyncio
import math
import datetime
import os
from asyncevents import emit
from pyrogram import idle, Client
from pyrogram.enums import ParseMode
from pyrogram.session import Session
from pyrogram.raw import functions
from shadowhawk.utils.Logging import log_chat
from shadowhawk import (
	loop,
	apps,
	slave,
	app_user_ids,
	session,
	loads,
	config,
	statistics,
	server_support,
	database,
	Parser,
	sessions_path,
	sessions_test_path,
	plmgr
)

from shadowhawk.utils.PluginManager import PluginManager

# Calculate the load avg magic numbers
load1_magic = math.exp(-5.0 / (1 * 60))
load5_magic = math.exp(-5.0 / (5 * 60))
load15_magic = math.exp(-5.0 / (15 * 60))
load30_magic = math.exp(-5.0 / (30 * 60))

async def main():
	async def _start_app(app):
		await app.start()
		asyncio.create_task(_get_me_loop(app))

	async def _get_me_loop(app):
		while True:
			try:
				me = await app.get_me()
				app_user_ids[me.id] = me
			except:
				pass
			await asyncio.sleep(60)

	async def _get_load_loop():
		while True:
			try:
				# get total pending tasks
				tasks = len(asyncio.all_tasks())
				# calculate 1 minute load
				loads[1] *= load1_magic
				loads[1] += tasks * (1 - load1_magic)
				# calculate 5 minute load
				loads[5] *= load5_magic
				loads[5] += tasks * (1 - load5_magic)
				# calculate 15 minute load
				loads[15] *= load15_magic
				loads[15] += tasks * (1 - load15_magic)
				# calculate 30 minute load
				loads[30] *= load30_magic
				loads[30] += tasks * (1 - load30_magic)
			except:
				# this exception will screw up the measurement but oh well.
				pass
			# our sample rate is 5 seconds for the magics
			await asyncio.sleep(5)

	# Set our start time
	statistics["start"] = datetime.datetime.now()

	# Initialize the slave, this doesn't use the plugin manager (yet)
	slave.set_thing(Client(
		"ShadowHawk-slave",
		api_id=config["telegram"]["api_id"],
		api_hash=config["telegram"]["api_hash"],
		plugins={"root": os.path.join(__package__, "slave-plugins")},
		parse_mode=ParseMode.HTML,
		bot_token=config["telegram"]["slave_bot_token"],
		workdir="sessions",
	))
	slave.parser = Parser(slave.get_thing())

	# Load the plugin manager and all it's plugins
	plmgr.set_thing(PluginManager(os.path.join(__package__, "plugins")))

	# Load the database after all the plugins are loaded.
	await database.innit()

	# Create client sessions after the event loop has started
	for i, session_name in enumerate(config["config"]["sessions"]):
		testserver = False
		workdir = str(sessions_path)
		if session_name.startswith("test:"):
			sessions_test_path.mkdir(exist_ok=True)
			session_name = session_name[5:]
			testserver = True
			workdir = str(sessions_test_path)
		
		app = Client(
			name=session_name,
			api_id=config["telegram"]["api_id"],
			api_hash=config["telegram"]["api_hash"],
			parse_mode=ParseMode.HTML,
			workdir=workdir,
			test_mode=testserver,
		)
		app.parser = Parser(app)
		# Temporarily assign a sequential integer while we're starting up.
		apps[i] = app
		plmgr.RegisterClient(app)

	
	# Start the telegram clients
	await asyncio.gather(*(_start_app(app) for app in apps.values()), slave.start())
	# Start the loops for our data
	asyncio.create_task(_get_load_loop())
	# asyncio.create_task(_send_log_loop())
	# Update the apps dictionary
	stevejobs = set(apps.values())
	apps.clear()
	for app in stevejobs:
		app_me = await app.get_me()
		apps[app_me.id] = app
	
	# Get some basic info on telegram limits I suppose.
	server_support.set_thing(
		await list(apps.values())[0].invoke(functions.help.GetConfig())
	)
	# Send a log message to the log chat saying the bot started, make it
	# a bit more informative than "lmao we started!"
	start_msg = "<b>=-=-= [ShadowHawk] =-=-=</b>\n"
	start_msg += f"<b>Current DC:</b> <code>{server_support.this_dc}{'[TEST]' if server_support.test_mode else ''}</code>\n"
	start_msg += f"<b>Max Supergroup Size:</b> <code>{server_support.megagroup_size_max}</code>\n"
	start_msg += f"<b>Max Group Size:</b> <code>{server_support.chat_size_max}</code>\n"
	start_msg += f"<b>Max Pinned Chats:</b> <code>{server_support.pinned_dialogs_count_max}</code>\n"
	start_msg += f"<b>Max Pinned Chats In Folders:</b> <code>{server_support.pinned_infolder_count_max}</code>\n"
	start_msg += f"<b>Max Forwarded Messages:</b> <code>{server_support.forwarded_count_max}</code>\n"
	start_msg += (
		f"<b>Max Saved GIFs:</b> <code>{server_support.saved_gifs_limit}</code>\n"
	)
	start_msg += f"<b>Max Recent Stickers:</b> <code>{server_support.stickers_recent_limit}</code>\n"
	start_msg += f"<b>Max Favorited Stickers:</b> <code>{server_support.stickers_faved_limit}</code>\n"
	start_msg += (
		f"<b>Max Caption Length:</b> <code>{server_support.caption_length_max}</code>\n"
	)
	start_msg += (
		f"<b>Max Message Length:</b> <code>{server_support.message_length_max}</code>\n"
	)
	start_msg += f"<b>GIF Search Username:</b> @{server_support.gif_search_username}\n"
	start_msg += (
		f"<b>Venue Search Username:</b> @{server_support.venue_search_username}\n"
	)
	start_msg += (
		f"<b>Image Search Username:</b> @{server_support.img_search_username}\n"
	)
	await log_chat(start_msg)
	# Announce that the bot has started, wait for all plugins to finish first.
	await emit("OnStart", True)
	# Idle forever.
	await idle()
	await emit("OnStop", block=True)
	await asyncio.gather(*(app.stop() for app in apps.values()), slave.stop())
	await session.close()

# Start the event loop and the bot.
Session.notice_displayed = True
loop.run_until_complete(main())