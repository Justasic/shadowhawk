from sqlalchemy import create_engine, MetaData
from shadowhawk import config
from asyncevents import emit

import ormar, databases

# Start up ormar
database = databases.Database(config["sql"]["uri"], min_size=1, max_size=config["sql"]["poolsize"])
metadata = MetaData()

# Ormar base model
class BaseModel(ormar.ModelMeta):

	# Set the Ormar stuff
	metadata = metadata
	database = database

# we're british I guess
async def innit():
	# Initialize SQL

	# Start up sql alchemy
	ormar_sqlengine = create_engine(
		config["sql"]["uri"],
		pool_size=config["sql"]["poolsize"],
		echo=config["sql"]["debug"],
	)

	# Connect?
	if not database.is_connected:
		await database.connect()

	# Create the tables if they don't already exist
	metadata.create_all(ormar_sqlengine)

	await emit("OnDatabaseStart", block=False)
	return True
