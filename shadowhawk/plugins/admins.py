from pyrogram import Client, filters
from pyrogram.enums import ChatMembersFilter
from shadowhawk import config, slave
from shadowhawk.utils import get_entity, self_destruct, name_escape, build_name_flags
from shadowhawk.utils.Logging import log_errors, public_log_errors

__help_section__ = "Info"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["admin", "admins"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def admins(client, message):
	"""{prefix}admins <i>[chat]</i> - Lists the admins in <i>[chat]</i>
Aliases: {prefix}admin
	"""
	chat, entity_client = message.chat, client
	command = message.command
	command.pop(0)
	silent = False
	list_ids = False
	ids_list = []
	if "-p" in command:
		command.remove("-p")
		silent = True
	if "-id" in command:
		command.remove("-id")
		list_ids = True
	if command:
		chat = " ".join(command)
		try:
			chat = int(chat)
		except ValueError:
			pass
		try:
			chat, entity_client = await get_entity(client, chat)
		except:
			await self_destruct(message, "<code>Invalid chat or group</code>")
			return
	text_unping = text_ping = ""
	async for i in entity_client.get_chat_members(chat.id, filter=ChatMembersFilter.ADMINISTRATORS):
		text_unping += f"\n[<code>{i.user.id}</code>] {await build_name_flags(client, i, ping=False, include_id=False)}"
		text_ping += f"\n[<code>{i.user.id}</code>] {await build_name_flags(client, i, ping=True, include_id=False)}"
		if i.title:
			OK = "\u200B"
			text_unping += f' // {name_escape(i.title.replace("@", f"@{OK}"))}'
			text_ping += f" // {name_escape(i.title)}"
		ids_list.append(i.user.id)
	if silent:
		await slave.send_message(
			message.from_user.id, text_ping, disable_web_page_preview=True
		)
		await message.delete()
	else:
		if list_ids:
			text_unping += "\n\n<b>IDs:</b>\n" + " ".join(map(str, ids_list))
			text_ping += "\n\n<b>IDs:</b>\n<code>" + " ".join(map(str, ids_list)) + "</code>"
		reply = await message.reply_text(text_unping, disable_web_page_preview=True)
		if text_unping != text_ping:
			await reply.edit_text(text_ping, disable_web_page_preview=True)

__signature__ = "SHSIG-IMcwQfZPRFIqZfNoQFeVwEzaGNkBLsN7rFN8CG8SMSapAAAAICB9qnaCLlAmTJIIj1Hmgr2gKpz0ijjrU9igeNh9JaVRAAAA"
