import asyncio, ormar
from asyncevents import on_event
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk.utils import get_entity, self_destruct
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk.database import BaseModel

__help_section__ = "Miscellaneous"

f = filters.chat([])
initted = False


class AutoScroll(ormar.Model):
	class Meta(BaseModel):
		tablename = "AutoScroll"

	id: int = ormar.BigInteger(primary_key=True)


@on_event("OnDatabaseStart")
async def __init_autoscroll(_, EventName: str):
	global initted
	if not initted:
		initted = True
		chats = await AutoScroll.objects.all()
		for a in chats:
			f.add(a.id)


@Client.on_message(f)
async def auto_read(client, message):
	await client.read_chat_history(message.chat.id)
	message.continue_propagation()


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["as", "autoscroll"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def autoscroll(client, message):
	"""{prefix}autoscroll <i>[channel id]</i> - Automatically mark chat messages as read
Aliases: {prefix}as
	"""
	command = message.command
	command.pop(0)
	chat = message.chat.id
	if command:
		chat = command[0]

	try:
		chat, entity_client = await get_entity(client, chat)
	except:
		await self_destruct("<code>Invalid chat or group</code>")
		return

	lel = await AutoScroll.objects.get_or_none(id=chat.id)

	if chat.id in f:
		f.remove(chat.id)
		if lel:
			await lel.delete()
		await message.edit(f"<code>Autoscroll disabled in {chat.title}</code>")
	else:
		f.add(chat.id)
		if not lel:
			lel = AutoScroll(id=chat.id)
			await lel.save()
		await message.edit(f"<code>Autoscroll enabled in {chat.title}</code>")
	await asyncio.sleep(3)
	await message.delete()


__signature__ = "SHSIG-IFcPycNFGD2uQIWeuFL/xmXmUmv2blKsvra/ubXYeazKAAAAIFV9xGOZPxIY+UKNxG2dY7JLu4uMuxp5vpluUuHA9OAkAAAA"