import html, asyncio
from pyrogram import Client, filters
from pyrogram.enums import ChatType
from shadowhawk import config
from shadowhawk.utils import get_entity
from shadowhawk.utils.Logging import log_errors, public_log_errors, log_chat

__help_section__ = "Block"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(["block"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def block(client, message):
	"""{prefix}block <i>[user id]</i> - Blocks the user either by reply or by id"""
	entity = message.chat
	command = message.command
	command.pop(0)
	if command:
		entity = " ".join(command)
	elif not getattr(message.reply_to_message, "empty", True):
		entity = message.reply_to_message.from_user or message.reply_to_message.chat
	entity, entity_client = await get_entity(client, entity)

	if not command and entity.type != ChatType.PRIVATE:
		await message.edit(
			f"<code>I can't block {entity.title} because it's not a private chat you retard</code>"
		)
		await asyncio.sleep(3)
		await message.delete()
		return

	try:
		if await client.block_user(entity.id):
			user_text = entity.first_name
			if entity.last_name:
				user_text += f" {entity.last_name}"
			user_text = html.escape(user_text or "Empty???")
			user_text += f" <code>[{entity.id}]</code>"
			await log_chat(
				"<b>User Block Event</b> [#BLOCKED]\n- <b>User:</b> " + user_text
			)
		else:
			await message.edit(f"<code>I cannot block {entity.title}</code>")
			await asyncio.sleep(3)
	except:
		await message.edit(f"<code>I cannot block {entity.title}</code>")
		await asyncio.sleep(3)

	await message.delete()


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(["unblock"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def unblock(client, message):
	"""{prefix}unblock <i>[user id]</i> - Unblocks the user either by reply or by id"""
	entity = message.chat
	command = message.command
	command.pop(0)
	if command:
		entity = " ".join(command)
	elif not getattr(message.reply_to_message, "empty", True):
		entity = message.reply_to_message.from_user or message.reply_to_message.chat
	entity, entity_client = await get_entity(client, entity)

	if not command and entity.type != ChatType.PRIVATE:
		await message.edit(
			f"<code>I can't unblock {entity.title} because it's not a private chat you retard</code>"
		)
		await asyncio.sleep(3)
		await message.delete()
		return

	try:
		if await client.unblock_user(entity.id):
			user_text = entity.first_name
			if entity.last_name:
				user_text += f" {entity.last_name}"
			user_text = html.escape(user_text or "Empty???")
			user_text += f" <code>[{entity.id}]</code>"
			await log_chat(
				"<b>User Unblock Event</b> [#UNBLOCK]\n- <b>User:</b> " + user_text
			)
		else:
			await message.edit(f"<code>I cannot unblock {entity.title}</code>")
			await asyncio.sleep(3)
	except:
		await message.edit(f"<code>I cannot unblock {entity.title}</code>")
		await asyncio.sleep(3)
	await message.delete()



__signature__ = "SHSIG-ILaI1Im9hMSrX8dV69tbTTPYVTkzspL+6HZd15K4XIyZAAAAIBQbRL3MJ4bezLuDrVJHUJUbqHxa6Ynfw7LboezPZVEFAAAA"