from pyrogram import Client, filters, ContinuePropagation
from asyncevents import on_event
from shadowhawk import config, acm
from shadowhawk.database import BaseModel, database
from shadowhawk.utils.Logging import log_errors, public_log_errors, log_chat
from shadowhawk.utils import name_escape, self_destruct, get_entity, build_name_flags
from uuid import UUID
import html
import ormar
import traceback


class Federation(ormar.Model):
	class Meta(BaseModel):
		tablename = "Federations"

	uuid: UUID = ormar.UUID(primary_key=True)
	name: str = ormar.String(max_length=128)
	meta: bytes = ormar.LargeBinary(max_length=1073742000)


class FederationChat(ormar.Model):
	class Meta(BaseModel):
		tablename = "FederationChats"

	id: int = ormar.BigInteger(primary_key=True)
	meta: bytes = ormar.LargeBinary(max_length=1073742000)
	federation: Federation = ormar.ForeignKey(Federation)


class FederationBan(ormar.Model):
	class Meta(BaseModel):
		tablename = "FederationBan"

	userid: int = ormar.BigInteger(primary_key=True)
	chat: FederationChat = ormar.ForeignKey(FederationChat)
	federation: Federation = ormar.ForeignKey(Federation)
	reason: str = ormar.Text()
	meta: bytes = ormar.LargeBinary(max_length=1073742000)


# Check for banned users
async def check_fban(client, chat, user):
	try:
		# Start with checking if we're admin in the chat.
		if not chat or not chat.id or not user or not user.id:
			return

		# Skip for now
		if not database.is_connected:
			return

		# This orm is retarded with postgresql, check if the chat exists before doing more queries.
		fedchat = await FederationChat.objects.get_or_none(id=chat.id)
		if not fedchat:
			return

		# now do a query to check if the user is fedbanned.
		fedban = await FederationBan.objects.get_or_none(
			chat__id=chat.id, userid=user.id
		)
		# fedban = (await session.execute(select(FederationBan).where(FederationBan.user_id == user.id, FederationBan.chat.id == chat.id))).scalar_one_or_none()
		if fedban:
			if not await acm.Lookup(client, chat.id, user.id):
				return
			# If we find they're fedbanned in this chat, ban them.
			await client.kick_chat_member(chat_id=chat.id, user_id=user.id)
			# make a log message stating the ban was enforced.
			chat_text = "<b>Federation Ban Enforcement Event</b> [#FBANENFORCE]\n"
			chat_text += f"- <b>Chat:</b> {await build_name_flags(client, chat)}\n"
			chat_text += f"- <b>User:</b> {await build_name_flags(client, user)}\n"
			chat_text += f"- <b>Federation:</b> {fedban.federation.name} <code>{fedban.federation.uuid}</code>\n"
			chat_text += (
				f"- <b>Reason:</b> {html.escape(fedban.reason.strip()[:1000])}\n"
			)
			await log_chat(chat_text)
	except Exception as ex:
		print(traceback.format_exc())


# Check most of the updates for fbans
@Client.on_message()
async def check_fban_message(client, message):
	await check_fban(client, message.chat, message.from_user)
	raise ContinuePropagation


@on_event("OnUserJoin")
async def check_fban_join(_, EventName: str, client, user, chat):
	await check_fban(client, chat, user)


@on_event("OnAddedUser")
async def check_fban_add(_, EventName: str, client, user, chat, added, message_id):
	for a in added:
		await check_fban(client, chat, a)
	# also check adder
	await check_fban(client, chat, user)


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["fban"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def fban(client, message):
	txt = message.text


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["unfban", "funban"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def unfban(client, message):
	txt = message.text


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["fcheck"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def fcheck(client, message):
	txt = message.text


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["fjoin"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def fjoin(client, message):
	txt = message.text


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["fleave"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def fleave(client, message):
	txt = message.text


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["newfed"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def newfed(client, message):
	txt = message.text


__signature__ = "SHSIG-ILUxiEcfGo8CE6bTwv0uzxs7b+U/uDMHwN4BpR7werLdAAAAIAu7IIc5bulDuVINIJKPAU44j7cgd0dqjJjug6J0Eb+JAAAA"