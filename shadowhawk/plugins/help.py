import html
from pyrogram import Client, filters
from pyrogram.errors.exceptions.forbidden_403 import Forbidden
from pyrogram.errors.exceptions.bad_request_400 import ChatSendInlineForbidden
from shadowhawk import slave, config
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk import plmgr as PluginManager

__help_section__ = "Help"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command("help", prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def help(client, message):
	"""{prefix}help - Shows list of plugins
{prefix}help <i>&lt;plugin name&gt;</i> - Shows help for <i>&lt;plugin name&gt;</i>
Can also be activated inline with: @{bot} help
	"""
	bot = await slave.get_me()
	module = message.command
	module.pop(0)
	module = " ".join(module).lower().strip()
	results = await client.get_inline_bot_results(bot.username or bot.id, "help")
	for a, i in enumerate(results.results):
		if a:
			internal_name = i.id[5:].split("-")
			internal_name.pop()
			internal_name = "-".join(internal_name).lower().strip()
			external_name = i.title.lower().strip()
			if module in (internal_name, external_name):
				result = i
				break
	else:
		result = results.results[0]
	try:
		await message.reply_inline_bot_result(results.query_id, result.id)
	except (Forbidden, ChatSendInlineForbidden):
		if module:
			await message.reply_text(
				{
					"message": result.send_message.message,
					"entities": result.send_message.entities,
				},
				parse_mode="through",
			)
		else:
			text = "Avaliable plugins:\n"

			helpkeys = []
			for plug in PluginManager.plugins:
				helpkeys.append(plug.GetHelpSection())

			for i in sorted(helpkeys):
				text += f"- {html.escape(helpkeys[i])}\n"
			await message.reply_text(text)



__signature__ = "SHSIG-IM+LGRchEDhwxDjWb9ild9HobmKPw9s8HEhKxJ3jeBHLAAAAIBb9QFLdZEG5erTUWUt11BPohAZXv2J3CxorkzTJnRj1AAAA"