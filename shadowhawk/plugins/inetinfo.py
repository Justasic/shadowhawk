import html
import asyncio
from io import BytesIO
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk.utils.Logging import log_errors
from shadowhawk.plugins.shell import processes

__help_section__ = "Miscellaneous"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["dig"], prefixes=config["config"]["prefixes"])
)
@log_errors
async def dig_command(client, message):
	"""{prefix}dig <i>(domain name)</i> - Gives dig information (this is just an alias to <code>{prefix}sh dig +noall +comments +answer</code>)
	"""
	# dig +noall +comments +answer
	command = message.command
	command.pop(0)
	command = "dig +noall +comments +answer " + " ".join(command)
	process = await asyncio.create_subprocess_shell(
		command,
		stdin=None,
		stdout=asyncio.subprocess.PIPE,
		stderr=asyncio.subprocess.PIPE,
	)
	process.cmdline = command
	processes[process.pid] = process
	stdout, stderr = await process.communicate(None)

	text = ""
	if process.pid in processes:
		del processes[process.pid]
	stdout = stdout.decode().replace("\r", "").strip("\n").rstrip()
	stderr = stderr.decode().replace("\r", "").strip("\n").rstrip()
	if stderr:
		text += f"<code>{html.escape(stderr)}</code>\n"
	if stdout:
		text += f"<code>{html.escape(stdout)}</code>"

	# send as a file if it's longer than 4096 bytes
	if len(text) > 4096:
		out = stderr.strip() + "\n" + stdout.strip()
		f = BytesIO(out.strip().encode("utf-8"))
		f.name = "output.txt"
		await message.reply_document(
			f, caption=f"<b>Exit Code:</b> <code>{process.returncode}</code>"
		)
	else:
		await message.reply(text)

__signature__ = "SHSIG-IBVMeyGw586LeJpibu8IEn03NgT4rra45k9cA8gb0ZBUAAAAIFidUHCHvjQutrkqrWRsZARLa/WB7/koIQ602e+vXKF5AAAA"