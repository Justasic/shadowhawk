from pyrogram import Client, filters
from pyrogram.errors.exceptions.bad_request_400 import MessageNotModified
from shadowhawk import config, slave
from shadowhawk.utils import get_entity, self_destruct, name_escape, build_name_flags
from shadowhawk.utils.Command import parse_command, ResolveChatUser
from shadowhawk.utils.Logging import log_errors, public_log_errors

__help_section__ = "Info"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["info", "whois"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def info(client, message):
	"""{prefix}info <i>&lt;entity&gt;</i> - Get entity info
{prefix}info <i>(as reply to message)</i> - Get entity info of replied user
Aliases: {prefix}whois
	"""
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	entity = user if user else chat
	silent = False

	if "p" in owo.keys() or "private" in owo.keys():
		silent = True

	if "f" in owo.keys() or "forward" in owo.keys():
		reply = message.reply_to_message
		# There is always `forward_date` regardless of forward privacy
		if not reply.forward_date:
			await self_destruct(message, "Cannot get forwarding entity.")
			return
		elif not reply.forward_from:
			await self_destruct(message, "Cannot get user info, user has forward privacy enabled.")
			return
		else:
			entity = reply.forward_from

	try:
		entity, _ = await get_entity(client, entity)
	except Exception as ex:
		await message.reply_text(f"{type(ex).__name__}: {str(ex)}", parse_mode=None)
		return
	
	ZWS = "\u200B"
	reply = None
	for ping in range(0, 2):
		print(f"For loop: {ping}, bool:", bool(ping))
		ping = bool(ping)

		text = await build_name_flags(client, entity, ping)
		text += f"\n<b>ID:</b> <code>{entity.id}</code>"
		text += "" if not entity.dc_id else f"\n<b>DC ID:</b> {entity.dc_id}"
		text += "" if not entity.username else "\n<b>Username:</b> @" + ('' if ping else '\u200B') + f"{entity.username}"

		if entity.restrictions:
			restrictions = []
			for r in entity.restrictions:
				restrictions.append(f"{r.reason}-{r.platform}")
			text += f'\n<b>Restrictions:</b> {", ".join(restrictions)}'

		text += "" if not entity.members_count else f"\n<b>Members:</b> {entity.members_count}"
		text += "" if not entity.linked_chat else f"\n<b>Linked Chat:</b> {await build_name_flags(client, entity.linked_chat, ping)} [<code>{entity.linked_chat.id}</code>]"
		text += "" if not (entity.description or entity.bio) else f'\n<b>Description:</b>\n{name_escape((entity.description or entity.bio).replace("@", "@" if ping else f"@{ZWS}"))}'
		
		if silent:
			await message.delete()
			if not ping:
				continue
			await slave.send_message(message.from_user.id, text, disable_web_page_preview=True)
		else:
			if ping:
				try:
					await reply.edit_text(text, disable_web_page_preview=True)
				except MessageNotModified:
					pass
			else:
				reply = await message.reply_text(text, disable_web_page_preview=True)


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command("id", prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def id(client, message):
	"""{prefix}id <i>[maybe reply to message]</i> - Gets IDs"""
	silent = "-p" in message.command
	text_unping = "<b>Chat ID:</b>"
	if message.chat.username:
		text_unping = (
			f'<a href="https://t.me/{message.chat.username}">{text_unping}</a>'
		)
	text_unping += f" <code>{message.chat.id}</code>\n"
	text = "<b>Message ID:</b>"
	if message.link:
		text = f'<a href="{message.link}">{text}</a>'
	text += f" <code>{message.id}</code>\n"
	text_unping += text
	if message.from_user:
		text_unping += f'<b><a href="tg://user?id={message.from_user.id}">User ID:</a></b> <code>{message.from_user.id}</code>\n'
	text_ping = text_unping
	reply = message.reply_to_message
	if not getattr(reply, "empty", True):
		text_unping += "\n"
		text = "<b>Replied Message ID:</b>"
		if reply.link:
			text = f'<a href="{reply.link}">{text}</a>'
		text += f" <code>{reply.id}</code>\n"
		text_unping += text
		text_ping = text_unping
		if reply.from_user:
			text = "<b>Replied User ID:</b>"
			if reply.from_user.username:
				text = f'<a href="https://t.me/{reply.from_user.username}">{text}</a>'
			text += f" <code>{reply.from_user.id}</code>\n"
			text_unping += text
			text_ping += f'<b><a href="tg://user?id={reply.from_user.id}">Replied User ID:</a></b> <code>{reply.from_user.id}</code>\n'
		if reply.forward_from:
			text_unping += "\n"
			text = "<b>Forwarded User ID:</b>"
			if reply.forward_from.username:
				text = (
					f'<a href="https://t.me/{reply.forward_from.username}">{text}</a>'
				)
			text += f" <code>{reply.forward_from.id}</code>\n"
			text_unping += text
			text_ping += f'\n<b><a href="tg://user?id={reply.forward_from.id}">Forwarded User ID:</a></b> <code>{reply.forward_from.id}</code>\n'
		if getattr(reply, "document", None):
			text = "\n"
			text += f"<b>File ID:</b> {reply.document.file_id}\n"
			text += f"<b>File Unique ID:</b> {reply.document.file_unique_id}\n"
			text += f"<b>File Name:</b> {reply.document.file_name}\n"
			text += f"<b>File Size:</b> {reply.document.file_size}\n"
			text += f"<b>Mime Type:</b> {reply.document.mime_type}\n"
			text_unping += text
			text_ping += text
	if silent:
		await slave.send_message(
			message.from_user.id, text_ping, disable_web_page_preview=True
		)
		await message.delete()
	else:
		reply = await message.reply_text(text_unping, disable_web_page_preview=True)
		if text_unping != text_ping:
			await reply.edit_text(text_ping, disable_web_page_preview=True)

__signature__ = "SHSIG-ICScWGuYIS50pW7SnZuazlVmqZI5BbRWdYqfFbJ4dozaAAAAIJLvqd7gZpHusW+3+jQ2LwMZHC3CGNd61W3NtShZE2twAAAA"