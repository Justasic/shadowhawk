import asyncio
from collections import deque
from pyrogram import Client, filters
from pyrogram.enums import ChatType
from shadowhawk import config, app_user_ids
from shadowhawk.utils import build_name_flags, name_escape
from shadowhawk.utils.Logging import log_errors, log_chat

logged = deque(maxlen=50)
lock = asyncio.Lock()


@Client.on_message(
	~filters.chat(config["logging"]["regular"])
	& filters.incoming
	& filters.forwarded
	& (filters.group | filters.channel)
)
@log_errors
async def log_forwards(client, message):
	if not config["logging"].get("log_forwards"):
		return
	if getattr(message.from_user, "id", None) in app_user_ids:
		return
	for i in app_user_ids:
		if message.forward_from:
			if i == message.forward_from.id:
				forwardee = app_user_ids[i]
				break
		j = app_user_ids[i].first_name
		if app_user_ids[i].last_name:
			j += f" {app_user_ids[i].last_name}"
		if j == message.forward_sender_name:
			forwardee = app_user_ids[i]
			break
	else:
		return
	identifier = (message.chat.id, message.id)
	async with lock:
		if identifier in logged:
			return
		text = "<b>Forwarded Event</b> [#FORWARD]\n- <b>Chat:</b> "
		text += await build_name_flags(client, message.chat)
		text += "\n- <b>Forwarder:</b> "
		if message.chat.type != ChatType.CHANNEL:
			if message.from_user:
				text += await build_name_flags(client, message.from_user)
			elif message.sender_chat and message.sender_chat.id != message.chat.id:
				text += await build_name_flags(client, message.sender_chat)
			else:
				text += "Anonymous"

		text += f'\n- <b><a href="{message.link}">Message'
		mtext = (message.text or message.caption or "").strip()
		if mtext:
			text += ":"
		text += "</a></b>"
		if mtext:
			text += f" {name_escape(mtext.strip()[:2000])}"
		text += "\n- <b>Forwardee:</b> " + await build_name_flags(client, forwardee)
		await log_chat(text)
		logged.append(identifier)


__signature__ = "SHSIG-IBCPeHh1umNObqxkBnOwpJNJ6fYmSXhsbRZiLjlB4M34AAAAINZ+o3RmpA6y9S2QrYGKVdE4BJcsXhYqjzldKz+H7ncRAAAA"