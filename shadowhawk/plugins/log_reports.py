import asyncio
from collections import deque
from pyrogram import Client, filters
from shadowhawk import config, slave
from shadowhawk.utils import name_escape, build_name_flags
from shadowhawk.utils.Logging import log_errors, log_chat

reported = deque(maxlen=50)
lock = asyncio.Lock()


@Client.on_message(
	~filters.chat(config["logging"]["regular"])
	& filters.regex(r"(?:^|\s+)@admins?(?:$|\W+)|^[/!](?:report|admins?)(?:$|\W+)")
	& filters.group
)
@log_errors
async def log_reports(client, message):
	if not config["logging"]["log_reports"]:
		return

	# Ignore the slave forwards
	if not getattr(message.forward_from, "empty", True):
		if message.forward_from.id == (await slave.get_me()).id:
			return

	# Ignore bots, they never report.
	if message.from_user:
		if message.from_user.is_bot:
			return

	# don't echo reports from certain chats
	if message.chat.id in config["logging"]["ignore_chat_reports"]:
		return

	identifier = (message.chat.id, message.id)
	async with lock:
		if identifier in reported:
			return
		text = "<b>Report Event</b> [#REPORT]\n- <b>Chat:</b> "
		text += await build_name_flags(client, message.chat)

		if message.from_user:
			user_text = await build_name_flags(client, message.from_user)
		elif message.sender_chat and message.sender_chat.id != message.chat.id:
			user_text = await build_name_flags(client, message.sender_chat)
		else:
			user_text = "Anonymous"

		text += f"\n- <b>Reporter:</b> {user_text}\n"
		start, end = message.matches[0].span()
		text += f'- <b><a href="{message.link}">Report Message'
		mtext = (message.text or message.caption or "").strip()
		if start or end < len(mtext):
			text += ":"
		text += "</a></b>"
		if start or end < len(mtext):
			text += f" {name_escape(mtext.strip()[:1000])}"

		reply = message.reply_to_message
		if not getattr(reply, "empty", True):
			text += "\n- <b>Reportee:</b> "
			if reply.from_user:
				user_text = await build_name_flags(client, reply.from_user)
			elif reply.sender_chat and reply.sender_chat.id != reply.chat.id:
				user_text = await build_name_flags(client, reply.sender_chat)
			else:
				user_text = "Anonymous"

			text += f'{user_text}\n- <b><a href="{reply.link}">Reported Message'
			mtext = reply.text or reply.caption or ""
			if mtext.strip():
				text += ":"
			text += f"</a></b> {name_escape(mtext.strip()[:1000])}"
			reported.append((reply.chat.id, reply.id))
		await log_chat(text)
		reported.append(identifier)


__signature__ = "SHSIG-IPDF4g25A0GLMDECbK+9c2PcTOk9F95u5tHM+LafyS5LAAAAIBsYU4CNuXoIawSK9TxL5vQpKBgGPmAIl1jD7Oivv3r0AAAA"