import asyncio
from collections import deque
from pyrogram import Client, ContinuePropagation
from pyrogram.errors.exceptions.flood_420 import FloodWait
from pyrogram.raw.types import (
	UpdateNewChannelMessage,
	UpdateNewMessage,
	MessageService,
	PeerChat,
	PeerChannel,
	MessageActionChatAddUser,
	MessageActionChatJoinedByLink,
	PeerUser,
)
from shadowhawk import config, slave, app_user_ids, log_peer_ids
from shadowhawk.utils import get_entity, build_name_flags, name_escape
from shadowhawk.utils.Logging import log_errors, log_chat
from asyncevents import emit

admincache = {}
handled = deque(maxlen=50)
lock = asyncio.Lock()

def sexy_user_name(user):
	text = user.first_name
	if user.last_name:
		text += " " + user.last_name
	return f'{"<code>[DELETED]</code>" if user.deleted else name_escape(text or "Empty???")} [<code>{user.id}</code>]'

@Client.on_raw_update()
@log_errors
async def log_user_joins(client, update, users, chats):
	if isinstance(update, (UpdateNewChannelMessage, UpdateNewMessage)):
		message = update.message
		if isinstance(message, MessageService):
			action = message.action
			if isinstance(
				action, (MessageActionChatAddUser, MessageActionChatJoinedByLink)
			):
				if isinstance(message.peer_id, PeerChannel):
					chat_id = message.peer_id.channel_id
					sexy_chat_id = int("-100" + str(chat_id))
				elif isinstance(message.peer_id, PeerChat):
					chat_id = message.peer_id.chat_id
					sexy_chat_id = -chat_id
				else:
					raise ContinuePropagation

				# Don't log our own adds, it's annoying.
				if not log_peer_ids:
					log_peer_ids.append(
						await slave.resolve_peer(config["logging"]["spammy"])
					)
					log_peer_ids.append(
						await slave.resolve_peer(config["logging"]["regular"])
					)
				if message.peer_id in log_peer_ids:
					raise ContinuePropagation

				# Check if the user was a join or not.
				is_join = isinstance(action, MessageActionChatJoinedByLink)
				if not is_join:
					is_join = action.users == [
						getattr(message.from_id, "user_id", None)
					]

				# user join or user was added?
				text = f"<b>{'User Join Event</b> [#USERJOIN]' if is_join else 'User Add Event</b> [#USERADD]'}\n- <b>Chat:</b> "
				text += await build_name_flags(client, chats[chat_id])
				text += "\n"
				# atext = name_escape(chats[chat_id].title)
				# if getattr(chats[chat_id], 'username', None):
				# 	atext = f'<a href="https://t.me/{chats[chat_id].username}">{atext}</a>'
				# else:
				# 	atext = f'<a href="https://t.me/c/{chat_id}/{message.id}">{atext}</a>'

				# text += f"{atext} [<code>{sexy_chat_id}</code>]\n"

				async with lock:
					if (sexy_chat_id, message.id) not in handled:
						if isinstance(message.from_id, PeerUser):
							adder = await build_name_flags(
								client, users[message.from_id.user_id]
							)
						else:
							adder = "Anonymous"
						if is_join:
							text += f"- <b>User:</b> {adder}\n"
							
							await emit("OnUserJoin", block=False, client=client, user=users[message.from_id.user_id], chat=chats[chat_id])

							if config["logging"]["log_join_bios"]:
								try:
									entity, _ = await get_entity(
										client, message.from_id.user_id
									)
									if entity.bio:
										text += f"- <b>Bio:</b> {entity.bio}\n"
								except:
									pass
							if isinstance(action, MessageActionChatJoinedByLink):
								text += f"- <b>Inviter:</b> {await build_name_flags(client, users[action.inviter_id])}"
						else:
							text += f"- <b>Adder:</b> {adder}\n- <b>Added Users:</b>\n"

							# Iterate over all the users being added.
							addedlist = []
							for user in action.users:
								text += f"--- {await build_name_flags(client, users[user])}\n"
								addedlist.append(users[user])
								if users[user].id in app_user_ids:
									await log_chat(text)

							await emit("OnAddedUser", block=False, client=client, user=users[message.from_id.user_id], chat=chats[chat_id], added=addedlist, message_id=message.id)

							# ee.emit(
							# 	"OnAddedUser",
							# 	client,
							# 	users[message.from_id.user_id],
							# 	chats[chat_id],
							# 	addedlist,
							# 	message.id,
							# )

						# Process all the other stuff.
						while True:
							try:
								# Check the config.
								if is_join and not config["logging"]["log_user_joins"]:
									raise ContinuePropagation
								if (
									not is_join
									and not config["logging"]["log_user_adds"]
								):
									raise ContinuePropagation
								# Send our message.
								await log_chat(text, "spammy")
							except FloodWait as ex:
								await asyncio.sleep(ex.value + 1)
							else:
								break
						handled.append((sexy_chat_id, message.id))
						raise ContinuePropagation
	raise ContinuePropagation


__signature__ = "SHSIG-IN6qzDtlbyrBhRtpeoYrpIRbWCnKp4nh6tI1RNZgQKBxAAAAINI2T+eVENjAcId0kb2nkVQx6ARW/qo5PTZ/HaKEk+fBAAAA"