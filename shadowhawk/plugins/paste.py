from io import BytesIO

from pyrogram import filters, Client
from pyrogram.types import Message
from pyrogram.enums import ParseMode

from shadowhawk import config
from shadowhawk.utils.Logging import (
	log_errors,
	public_log_errors,
)

__help_section__ = "Paste"

from shadowhawk.utils.paste import paste


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["paste"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def paste_msg(_, msg: Message):
	"""{prefix}paste [args, reply] - paste the supplied data to the configured paste api
	"""
	txt = msg.text.split(" ", 1)[1] if len(msg.text.split(" ", 1)) > 1 else ""
	if rep := msg.reply_to_message:
		if rep.text:
			txt = rep.text
		elif rep.caption:
			txt = rep.caption
		elif rep.document and rep.document.file_size < 10 * (1024 ** 2):  # 10MB
			doc: BytesIO = await rep.download(in_memory=True)
			doc.seek(0)
			txt = doc.read().decode()
		else:
			return await msg.reply_text("I can't paste that.")
	if len(txt) > 0:
		url, _ = paste(txt)  # idk log the modification token or something
		if url:
			await msg.reply_text("Pasted successfully [Link]({})".format(url), parse_mode=ParseMode.MARKDOWN)
			return
	await msg.reply_text("Paste failed.")


__signature__ = "SHSIG-IEvsbSGP4XTpIWopaKNyF4iAyTMuX1Az1w7M3BpEZF1WAAAAICRP3obETIE+1gSloaeVWYMHQmGLDjRGhUzJkiQQPH9KAAAA"