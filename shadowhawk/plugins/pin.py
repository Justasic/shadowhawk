import asyncio

from pyrogram import Client, filters
from pyrogram.types import Message
from shadowhawk import config, acm
from shadowhawk.utils.Logging import log_errors, public_log_errors

# kanged from https://github.com/Dank-del/EagleX/blob/master/Bot/modules/pin.py

__help_section__ = "Pin"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["pin"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def pin_message(client, message: Message):
	"""{prefix}pin <i>(maybe reply to a message) [loud]</i> - Pins the replied message (use <code>loud</code> to notify all users)"""

	me = await client.get_me()
	# If you are an admin
	if await acm.Lookup(client, message.chat.id, me.id):
		# If you replied to a message so that we can pin it.
		if message.reply_to_message:
			disable_notification = True

			# Let me see if you want to notify everyone. People are gonna hate you for this...
			if len(message.command) >= 2 and message.command[1] in [
				"alert",
				"notify",
				"loud",
			]:
				disable_notification = False

			# Pin the fucking message.
			await client.pin_chat_message(
				message.chat.id,
				message.reply_to_message.id,
				disable_notification=disable_notification,
			)
			await message.edit("<code>Pinned message!</code>")
		else:
			# You didn't reply to a message and we can't pin anything. ffs
			await message.edit(
				"<code>Reply to a message so that I can pin the god damned thing...</code>"
			)
	else:
		# You have no business running this command.
		await message.edit(
			"<code>I am not an admin here lmao. What am I doing?</code>"
		)

	# And of course delete your lame attempt at changing the group picture.
	# RIP you.
	# You're probably gonna get ridiculed by everyone in the group for your failed attempt.
	# RIP.
	await asyncio.sleep(3)
	await message.delete()


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["unpin", "unpinall"], prefixes=config["config"]["prefixes"])
)
@log_errors
async def unpin_message(client, message: Message):
	"""{prefix}unpin <i>(maybe reply to a message)</i> - Unpins the replied message"""

	me = await client.get_me()
	# If you are an admin
	if await acm.Lookup(client, message.chat.id, me.id):
		# If you replied to a message so that we can unpin it.
		if message.reply_to_message:

			# unpin the fucking message.
			await client.unpin_chat_message(
				message.chat.id, message.reply_to_message.id
			)
			await message.edit("<code>Unpinned message!</code>")
		else:
			# You didn't reply to a message and we can't unpin anything. ffs
			await message.edit(
				"<code>Reply to a message so that I can unpin the god damned thing...</code>"
			)
	else:
		# You have no business running this command.
		await message.edit(
			"<code>I am not an admin here lmao. What am I doing?</code>"
		)

	# And of course delete your lame attempt at changing the group picture.
	# RIP you.
	# You're probably gonna get ridiculed by everyone in the group for your failed attempt.
	# RIP.
	await asyncio.sleep(3)
	await message.delete()


__signature__ = "SHSIG-IMVmY1I/XurYxAIzZRueXj6gWwGnqMOBD7xrbznH9Lw5AAAAICFjLqYPKUauMwTTOiZ4xEOGu4LiMlRxx5ADS36M0XE1AAAA"