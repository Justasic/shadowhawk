import time
from pyrogram import Client, filters
from datetime import datetime
from shadowhawk import config, loads
from shadowhawk.utils import get_duration
from shadowhawk.utils.Logging import (
	log_ring,
	spammy_log_ring,
	log_errors,
	public_log_errors,
)

__help_section__ = "Ping"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["ping", "pong"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def ping_pong(client, message):
	"""{prefix}ping - Pong!
{prefix}pong - Ping!
	"""
	strings = {"ping": "Pong!", "pong": "Ping!"}
	text = strings[message.command[0].lower()]
	start = time.time()
	reply = await message.reply_text(text)
	end = time.time()
	now = datetime.now()
	# Log the time between receiving the message and our response
	avgs = f"{loads[1]:.2f}, {loads[5]:.2f}, {loads[15]:.2f}, {loads[30]:.2f}"
	logring = f"{log_ring.qsize()}/{log_ring.maxsize}"
	spammyring = f"{spammy_log_ring.qsize()}/{spammy_log_ring.maxsize}"
	await reply.edit_text(
		f"{text}\n<i>{round((end-start)*1000)}ms</i>\n<b>Delta: </b><i>{get_duration((now-message.date).total_seconds())}</i>\n<b>Task Avg:</b> {avgs}\n<b>Log Ring:</b> {logring}\n<b>Spammy Ring:</b> {spammyring}"
	)

__signature__ = "SHSIG-IE767LYR3uItT18Xy3Mu5zGe8abiPI2+fQ7++vWMqbuSAAAAIGR/wVyyynyMfPT0tg6GYmNF6AA1vAtIBd26MyTfhAT8AAAA"