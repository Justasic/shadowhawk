import os
import signal
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk.utils.Logging import log_errors, public_log_errors

__help_section__ = "Poweroff"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(
		["poweroff", "shutdown", "stop"], prefixes=config["config"]["prefixes"]
	)
)
@log_errors
@public_log_errors
async def poweroff(client, message):
	"""{prefix}poweroff - Turns off the userbot
Aliases: {prefix}shutdown, {prefix}stop
	"""
	await message.reply_text("Goodbye")
	os.kill(os.getpid(), signal.SIGINT)

__signature__ = "SHSIG-IEMxYeOswFLIRIiAfl3fY0y1jK6XnV16BI2jVIv4zaBjAAAAIJU9eCxgorRbWivlBp2eazM61jOwRVj/d6xKTmvOd0RrAAAA"