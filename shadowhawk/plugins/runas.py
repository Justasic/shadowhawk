import asyncio
from pyrogram import Client, filters
from shadowhawk import config
from shadowhawk.utils import self_destruct, get_app, get_entity
from shadowhawk.utils.Logging import log_errors

__help_section__ = "Miscellaneous"

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(["rs", "runas"], prefixes=config["config"]["prefixes"])
)
@log_errors
async def runas(client, message):
	"""{prefix}runas <i>username</i> <i>(maybe reply to a message)</i> - Run the command as another account
Aliases: {prefix}rs
	"""
	reply = message.reply_to_message
	command = message.command
	command.pop(0)
	user = command[0]
	chat = message.chat.id
	msg = " ".join(command[1:])

	try:
		entity, _ = await get_entity(client, user)
	except:
		entity = None

	if not entity:
		await self_destruct(message, f'unknown user "{user}"')
		return

	try:
		app = await get_app(entity.id)
	except:
		app = None

	if not app:
		await self_destruct(
			message, f"{user} is not a user that shadowhawk is logged into."
		)
		return

	await message.delete()

	if reply:
		await app.send_message(chat, msg, reply_to_message_id=reply.id)
	else:
		await app.send_message(chat, msg)


yeetpurge_info = {True: dict(), False: dict()}
yeetpurge_lock = asyncio.Lock()


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(["multirun", "mr"], prefixes=config["config"]["prefixes"])
)
@log_errors
async def multirun(client, message):
	"""{prefix}multirun <i>(maybe reply to a message)</i> - replies to messages between messages (useful for running many commands at once)
Aliases: {prefix}mr
	"""
	reply = message.reply_to_message
	if getattr(reply, "empty", True):
		await message.delete()
		return

	info = yeetpurge_info[False]
	async with yeetpurge_lock:
		if message.from_user.id not in info:
			info[message.from_user.id] = dict()
		info = info[message.from_user.id]
		if message.chat.id not in info:
			resp = await message.reply_text("Reply to end destination")
			info[message.chat.id] = (message, reply, resp)
			return
		og_message, og_reply, og_resp = info.pop(message.chat.id)

	deletion = set((og_message.id, message.id, og_resp.id))
	messages = set()
	from_id, to_id = sorted((og_reply.id, reply.id))
	async for i in client.get_history(message.chat.id, offset_id=to_id):
		if not i.outgoing:
			messages.add(i.id)
		if from_id >= i.id:
			break
	command = " ".join(message.command[1:])
	tasks = set()
	for m in messages:
		tasks |= set(
			(client.send_message(message.chat.id, command, reply_to_message_id=m),)
		)
	tasks |= set((client.delete_messages(message.chat.id, deletion),))
	asyncio.gather(*tasks)


__signature__ = "SHSIG-IKjMl+Gl3aeOdkFqBeAGHlDlDNZO2tk1thR/Y5zFMtuYAAAAIGCYFJ7iRtzEki+YjU5ltZvyNkOpuJVbLICVG5kW3wwYAAAA"