import asyncio
import typing
import html
import traceback
from datetime import datetime, timedelta

import SibylSystem
from pyrogram import Client, filters
from pyrogram.enums import ChatMembersFilter
from pyrogram.types import Message
from SibylSystem import PsychoPass
from SibylSystem.types import MultiBanInfo
from pyrogram.errors.exceptions.bad_request_400 import MessageNotModified, UsernameNotOccupied
from shadowhawk import config
from asyncevents import on_event
from shadowhawk.utils import get_entity, self_destruct, build_name_flags
from shadowhawk.utils.Command import ParseInviteLink, parse_command
from shadowhawk.utils.Logging import log_errors, public_log_errors, log_chat
from shadowhawk.plugins.moderation import ResolveChatUser

__help_section__ = "Sibyl"

sibyl_client: typing.Union[PsychoPass, None]

# Use OnDatabaseStart since it's fairly easy
# and we'll likely depend on database stuff anyway
@on_event("OnDatabaseStart")
async def OnStart(_, EventName: str):
	global sibyl_client
	try:
		sibyl_client = PsychoPass(config['config']['sibyl_api'], host=config['config']['sibyl_endpoint'] if 'sibyl_endpoint' in config['config'] else "https://psychopass.animekaizoku.com/", show_license=False)
	except Exception as ex:
		traceback.print_exception(ex)
		sibyl_client = None


raidscan_info = {True: dict(), False: dict()}
raidscan_lock = asyncio.Lock()


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(["raid", "raidscan"], prefixes=config["config"]["prefixes"])
)
@log_errors
async def raid(client: Client, message: Message):
	"""{prefix}raidscan <i>(reply to a message)</i> - scans users between 2 messages
	arg -b or -ban - bans the user temporarily
	arg -d or -delete - deletes the messages
Aliases: {prefix}raid
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	owo = parse_command(message.text)
	reason = next((val for key, val in owo.items() if key in ["r", "reason"]), None)
	doban = ("ban" or "b") in owo
	delete = ("del" or "d") in owo
	reply = message.reply_to_message
	if getattr(reply, "empty", True):
		await message.delete()
		return
	chat = message.chat
	info = raidscan_info[False]
	async with raidscan_lock:
		if message.from_user.id not in info:
			info[message.from_user.id] = dict()
		info = info[message.from_user.id]
		if message.chat.id not in info:
			resp = await message.reply_text("Reply to end destination")
			info[message.chat.id] = (message, reply, resp)
			return
		og_message, og_reply, og_resp = info.pop(message.chat.id)
	do_ban = set()
	try:
		ban = []
		delmsg = []
		txt = "<b>Sibyl Raid Ban</b> \n[#SIBYLBAN #RAID]\n"
		chattext = await build_name_flags(client, chat)
		txt += f"\n- <b>Chat:</b> {chattext}\n"
		from_id, to_id = sorted((og_reply.id, reply.id))
		async for i in client.get_chat_history(message.chat.id, offset_id=to_id+1):
			if not i.outgoing and i.service:
				i: Message
				ma = MultiBanInfo(
					user_id=i.from_user.id,
					is_bot=i.from_user.is_bot,
					reason=reason,
					message=reply.text if reply else "",
					source=message.link,
					source_group="https://t.me/{}".format(f"c/{str(chat.id).replace('-100', '')}")
				)
				ban.append(ma)
				txt += "\n- <b>User:</b> " + await build_name_flags(client, i.from_user)
				if ban:
					do_ban.add(i.from_user.id)
				if delete:
					delmsg.append(i.id)
			if from_id >= i.id:
				break
		final_text = f"Scanned [<code>{len(ban)}</code>] people.\n<b>Reason</b>: \n<code>{reason}</code>\n<b>Sibyl:</b> \n"
		response = sibyl_client.multi_ban(info=ban)
		await self_destruct(message, final_text + f"<code>{response}</code>")
		txt += f"\n\n{final_text}<code>{response}</code>"
		await log_chat(txt, "bans")
		if delete:
			try:
				await client.delete_messages(chat_id=message.chat.id, message_ids=delmsg)
			except BaseException as er:
				await message.reply(f"Failed to delete due to <code>{er}</code>")
	except BaseException as e:
		await self_destruct(message, f"Failed due to <code>{e}</code>")
	if doban:
		for uid in do_ban:
			try:
				await message.chat.ban_member(uid, datetime.now() + timedelta(seconds=9000))
				await asyncio.sleep(5)
			except Exception:
				pass


@Client.on_message( ~filters.sticker & ~filters.via_bot & filters.me & filters.command(['asb', 'associationban', 'assban'], prefixes=config['config'][ 'prefixes']))
@log_errors
@public_log_errors
async def ass_ban(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}associationban [-c=&ltchat&gt|as reply] -r="Reason text" - Ban a group of users by association
Aliases: {prefix}assban, {prefix}asb
Flags: -i -ignore -noadmins -na: ignore admins
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	reply = message.reply_to_message
	text = "<b>Sibyl Association Ban</b> \n[#SIBYLBAN #ASSOSIATION]\n"
	chat = None

	owo = parse_command(message.text)
	if key_chat := next(
			(key for key, _ in owo.items()
			 if key in ["c", "chat", "group", "g", "channel"]),
			None,
	):
		try:
			ent, _ = await get_entity(client, owo[key_chat])
			chat = ent
		except UsernameNotOccupied:
			chat = None
		except Exception as e:
			await self_destruct(message, f"Failed to resolve chat due to <code>{e}</code>")
			return

	reason = next((val for key, val in owo.items() if key in ["r", "reason"]), None)
	adders = next((val for key, val in owo.items() if key in ["a", "adders", "u", "users"]), None)
	ignore_admins = next((val for key, val in owo.items() if key in ["i", "ignore", "noadmins", "na"]), None)

	if not chat:
		await self_destruct(message, "<code>Cannot find the target chat</code>")
		return

	if not reason:
		await self_destruct(message, "<code>You must specify a reason</code>")
		return

	try:
		mass_adders = []
		d = []
		resolved = []
		chattext = await build_name_flags(client, chat)
		text += f"\n- <b>Chat:</b> {chattext}"
		text += "\n-----------------------\n"
		if adders:
			for a in adders:
				if not a.isnumeric():
					a = a.splt("/")[-1]
				if int(a) not in resolved:
					resolved.append(int(a))
			try:
				text += f"- <b>Adders:</b>"
				text += "\n-----------------------"
				for item in await client.get_messages(chat.id, resolved):
					u_adder = item.from_user
					ma = MultiBanInfo(
						user_id=u_adder.id,
						is_bot=u_adder.is_bot,
						reason=reason,
						message=reply.text if reply else "",
						source=message.link,
						source_group="https://t.me/{}".format(f"c/{str(chat.id).replace('-100', '')}")
					)
					mass_adders.append(u_adder.id)
					d.append(ma)
					text += "\n- <b>Adder:</b> " + await build_name_flags(client, u_adder)
			except Exception as e:
				await log_chat(str(e))
		text += "\n-----------------------"
		if not ignore_admins:
			text += f"\n- <b>Admins:</b>"
			text += "\n-----------------------"
			async for mem in client.get_chat_members(chat.id, filter=ChatMembersFilter.ADMINISTRATORS):
				if not mem.user.is_bot and mem.user.id not in mass_adders:
					if mem.status == "creator":
						owo = MultiBanInfo(
							user_id=mem.user.id,
							is_bot=mem.user.is_bot,
							reason=f"chat creator where {reason}",
							message=reply.text if reply else "",
							source=message.link,
							source_group="https://t.me/{}".format(
								chat.username or f"c/{str(chat.id).replace('-100', '')}")
						)
						d.append(owo)
						text += "\n- <b>Creator:</b> " + await build_name_flags(client, mem.user)
					else:
						owo = MultiBanInfo(
							user_id=mem.user.id,
							is_bot=mem.user.is_bot,
							reason=f"admins where {reason}",
							message=reply.text if reply else "",
							source=message.link,
							source_group="https://t.me/{}".format(f"c/{str(chat.id).replace('-100', '')}")
						)
						d.append(owo)
						text += "\n- <b>Admin:</b> " + await build_name_flags(client, mem.user)
		text += "\n-----------------------\n"
		final_text = f"Scanned from source [<code>{len(d)}</code>] people.\n<b>Reason</b>: \n<code>{reason}</code>\n<b>Sibyl:</b> \n"
		response = sibyl_client.multi_ban(info=d)
		await self_destruct(message, final_text + f"<code>{response}</code>")
		text += f"\n{final_text}<code>{response}</code>"
		await log_chat(text)
	except BaseException as e:
		await self_destruct(message, f"Failed due to <code>{e}</code>")


@Client.on_message( ~filters.sticker & ~filters.via_bot & filters.me & filters.command(['asub', 'associationunban', 'assunban'], prefixes=config['config'][ 'prefixes']))
@log_errors
@public_log_errors
async def ass_unban(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}associationunban [-u=&ltuser&gt|as reply|as mention] [-c=&ltchat&gt|as reply] - Remove an association ban
Aliases: {prefix}assunban, {prefix}asub
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	owo = parse_command(message.text)
	_, chat = await ResolveChatUser(owo, client, message)

	if not chat:
		await self_destruct(message, "<code>Cannot find the target chat</code>")
		return
	
	try:
		d = []
		for mem in await client.get_chat_members(chat.id, filter=ChatMembersFilter.ADMINISTRATORS):
			if not mem.user.is_bot:
				d.append(mem.user.id)
				text = "<b>Sibyl Unban Event</b> [#SIBYLUNBAN]"
				text += "\n- <b>Chat:</b> " + await build_name_flags(client, chat)
				text += "\n- <b>Unbanned:</b> " + await build_name_flags(client, mem.user.id)
				await log_chat(text)
		response = sibyl_client.multi_unban(d)
		await message.reply_text(f"Unbanned {len(d)} people\nSibyl: <code>{response}</code>")
	except BaseException as e:
		await self_destruct(message, f"Failed due to <code>{e}</code>")


@Client.on_message( ~filters.sticker & ~filters.via_bot & filters.me & filters.command(['elm', 'eliminate'], prefixes=config['config'][ 'prefixes']))
@log_errors
@public_log_errors
async def ban_user(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}eliminate [-u=&ltuser&gt|as reply|as mention] -r="Reason text" - Use the Lethal Eliminator mode of your Dominator
Aliases: {prefix}elm
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	owo = parse_command(message.text)
	user, _ = await ResolveChatUser(owo, client, message)

	reason = next((val for key, val in owo.items() if key in ["r", "reason"]), None)

	if not user:
		await self_destruct(message, "<code>Cannot find the target user</code>")
		return

	if not reason:
		await self_destruct(message, "<code>You must specify a reason</code>")
		return

	try:
		sibyl_client.add_ban(user.id, reason=reason, source=message.link)
		nametext = await build_name_flags(client, user)
		text = "<b>Sibyl Ban Event</b> [#SIBYLBAN]"
		text += "\n- <b>Banned:</b> " + nametext
		text += f"\n- <b>Reason:</b> <code>{html.escape(reason)}</code>"
		await log_chat(text)
		await self_destruct(message, f"{nametext} was eliminated with reason <code>{reason}</code>")
	except BaseException as e:
		await self_destruct(message, f"Failed due to <code>{e}</code>")


@Client.on_message( ~filters.sticker & ~filters.via_bot & filters.me & filters.command(['rev', 'revive'], prefixes=config['config'][ 'prefixes']))
@log_errors
@public_log_errors
async def unban_user(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}revive [-u=&ltuser&gt|as reply|as mention] - Revive a user who was lethally eliminated
Aliases: {prefix}rev
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	owo = parse_command(message.text)
	user, _ = await ResolveChatUser(owo, client, message)
	
	try:
		sibyl_client.delete_ban(user.id)
		nametext = await build_name_flags(client, user)
		text = "<b>Sibyl Unban Event</b> [#SIBYLUNBAN]"
		text += "\n- <b>Unbanned:</b> " + nametext
		await log_chat(text)
		await message.reply(f"{nametext} was revived")
	except BaseException as e:
		await self_destruct(message, f"Failed due to <code>{e}</code>")


@Client.on_message(~filters.sticker & ~filters.via_bot & filters.me & filters.command(
		['rscan'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def remote_scan_user(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}rscan [message link] Reason text - Remote scan a user to SibylSystem
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	if len(message.text.split()) < 3:
		return await self_destruct(message, "Insufficient args!\nUsage: <code>{}rscan [message link] Reason text</code>".format(config['config']['prefixes'][0]))

	user, target_chat, target_msg, _ = await ParseInviteLink(message, client)
	reason = message.text.split(" ", 2)[2]

	if not user:
		await self_destruct(message, "<code>Cannot find the target user</code>")
		return

	if not target_chat:
		await self_destruct(message, "<code>Cannot find the target chat</code>")
		return

	if not reason:
		await self_destruct(message, "<code>You must specify a reason</code>")
		return

	msg = target_msg.text or target_msg.caption or ""

	try:
		sibyl_client.add_ban(user.id, reason=reason, message=msg, source=message.link)

		nametext = await build_name_flags(client, user)
		text = "<b>Sibyl Ban</b> [#SIBYLBAN #REMOTE]"
		text += "\n- <b>Banned:</b> " + nametext
		text += '\n- <b>Source:</b> ' + await build_name_flags(client, target_chat)
		text += f"\n- <b>Reason:</b> <code>{html.escape(reason)}</code>"
		await log_chat(text, "bans")
		await message.reply(f"{nametext} was scanned!\nReason <code>{reason}</code>")
	except BaseException as e:
		await message.reply(f"Failed due to <code>{e}</code>")


@Client.on_message( ~filters.sticker & ~filters.via_bot & filters.me & filters.command(['cymatic', 'cymaticscan', 'cyscan'], prefixes=config['config'][ 'prefixes']))
@log_errors
@public_log_errors
async def lookup_user(client: Client, message: Message) -> typing.Optional[None]:
	"""{prefix}cymaticscan [-u=&ltuser&gt|as reply|as mention] - Query information on a user
Aliases: {prefix}cymatic, {prefix}cyscan
	"""
	global sibyl_client
	if not sibyl_client:
		await self_destruct(message, "<code>This feature won't work due to lack of Sibyl API connection</code>")
		return

	owo = parse_command(message.text)
	user, _ = await ResolveChatUser(owo, client, message)
	
	info = officer_info = None
	try:
		info = sibyl_client.get_info(user.id)
	except SibylSystem.exceptions.GeneralException as ex:
		await self_destruct(message, f"<code>Error: {ex}</code>")
		return

	# Determine if we need to make a stupid "get more info" call
	if info.crime_coefficient <= 10 or (info.crime_coefficient > 100 and info.crime_coefficient <= 150):
		officer_info = sibyl_client.get_general_info(user.id)
		if officer_info and officer_info.result:
			officer_info = officer_info.result

	# Try and get the enforcer
	officer = None
	try:
		officer, _ = await get_entity(client, info.banned_by)
	except ValueError:
		pass

	reply = None
	for ping in range(0, 2):
		text = "<b>Cymatic Scan Results</b>\n"
		text += await build_name_flags(client, user, ping=bool(ping))
		text += "" if not info.crime_coefficient else f"\n- <b>Crime Coefficient:</b> <code>{info.crime_coefficient}</code>"
		text += "" if not info.hue_color else f"\n- <b>Hue:</b> <code>{info.hue_color}</code>"
		# text += "" if not info.date else f"\n- <b>Record Date:</b> <code>{info.date}</code>"
		text += "" if not info.is_bot else "\n- <b>Bot:</b> <code>Yes</code>"
		text += "" if not info.banned else "\n- <b>Banned:</b> <code>Yes</code>"
		text += "" if not info.reason else f"\n- <b>Ban Reason:</b> <code>{info.reason}</code>"
		text += "" if not info.banned_by else f"\n- <b>Ban Enforcer:</b> {await build_name_flags(client, officer, ping=bool(ping))}"
		text += "" if not info.ban_source_url else f"\n- <b>Ban Source:</b> <code>{info.ban_source_url}</code>"
		text += "" if not info.ban_flags else f"\n- <b>Ban Flags:</b> <code>{' '.join(info.ban_flags)}</code>"
		text += "" if not info.source_group else f"\n- <b>Source Group:</b> <code>{info.source_group}</code>"
		
		if officer_info:
			text += "" if not officer_info.division else f"\n- <b>Divison:</b> <code>{officer_info.division}</code>"
			text += "" if not officer_info.assigned_by else f"\n- <b>Assigned By:</b> {await build_name_flags(client, officer, ping=bool(ping))}"
			text += "" if not officer_info.assigned_reason else f"\n- <b>Reason for Assignment:</b> <code>{officer_info.assigned_reason}</code>"
			text += "" if not officer_info.assigned_at else f"\n- <b>Assignment Date:</b> <code>{officer_info.assigned_at}</code>"
			text += "" if not officer_info.permission else f"\n- <b>Position:</b> <code>{officer_info.permission.name.title()}</code>"
		
		if bool(ping):
			try:
				await reply.edit(text, disable_web_page_preview=True)
			except MessageNotModified:
				pass
		else:
			reply = await message.reply(text, disable_web_page_preview=True)

__signature__ = "SHSIG-IAD29qzMs1p/RF3qPp4kG4ZTZH+2PeXZ5n872OYQr4svAAAAIEepFDYJzSTqqatibXS1zNyDx1uVz20uz98AftwiiaiQAAAA"