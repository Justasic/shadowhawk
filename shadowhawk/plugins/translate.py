from pyrogram import Client, filters
from pyrogram.types import Message
from gpytranslate import Translator
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk import config

__help_section__ = "Translate"

ZWS = "\u200B"
t = Translator()

@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& filters.me
	& filters.command(["tr", "translate"], prefixes=config["config"]["prefixes"])
)
@log_errors
@public_log_errors
async def translate(client: Client, message: Message) -> None:
	"""{prefix}translate <i>(as reply to text)</i> <i>[src]:[dest]</i> - Translates text and stuff
Aliases: {prefix}tr
	"""

	if not message.reply_to_message or (not message.reply_to_message.text and not message.reply_to_message.caption):
		await message.reply_text("Reply required")
		return

	text = message.reply_to_message.text or message.reply_to_message.caption

	src_lang = "auto"
	dest_lang = "en"
	lang = " ".join(message.command[1:])

	lang = lang.split(":", 1)
	if len(lang) == 1 or not lang[-1]:
		dest_lang = lang.pop(0) or dest_lang
	else:
		dest_lang = lang.pop(0)

	
	result = await t.translate(text, sourcelang=src_lang, targetlang=dest_lang)
	lang = await t.detect(text)

	if result.text == text:
		await message.reply_text("They're the same")
	else:
		reply = None
		for ping in range(0, 2):
			ping = bool(ping)

			text = f"Translated from {lang} to {dest_lang}:\n{result.text[:4000] if ping else result.text.replace('@', '@' + ZWS)[:4000]}"

			if ping:
				if reply.text != text:
					reply.edit_text(text, parse_mode=None, display_web_page_preview=True)
			else:
				reply = await message.reply(text, parse_mode=None, disable_web_page_preview=True)


__signature__ = "SHSIG-IMuqoHt1G9Bm7nr8dFggpQQXOecDWkOFPRZoT2QwR3v6AAAAICnu2tWwP0+liFJJvJ56Ynf69aDQo5lT1WlLN9zzeicOAAAA"