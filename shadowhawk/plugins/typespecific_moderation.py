from pyrogram import filters, Client
from pyrogram.types import Message

from shadowhawk import config
from shadowhawk.utils import self_destruct
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk.utils.types import (
	Types,
	stringify_type,
	is_valid_match_type,
	matches_type,
)


__help_section__ = "TypeSpecific"


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(
		["types", "availabletypes"],
		prefixes=config["config"]["prefixes"],
	))
@log_errors
@public_log_errors
async def types(_: Client, message: Message):
	"""{prefix}types check a list of available type messages
Aliases: {prefix}availabletypes
"""
	await message.reply("The following types are available:\n\n" + stringify_type(Types))


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(
		["match"],
		prefixes=config["config"]["prefixes"],
	))
@log_errors
@public_log_errors
async def matches_types(_: Client, message: Message):
	"""{prefix}match <i>(as reply to a message)</i> checks if a message matches the type(s)
"""

	if not message.reply_to_message:
		await self_destruct(message, "Not replied to a message")
		return
	reply = message.reply_to_message

	if len(message.text.split(" ")) < 2:
		await self_destruct(message, "No type provided")
		return

	else:
		_type = message.text.split(" ")[1]

		if not is_valid_match_type(_type):
			await self_destruct(message, "{} is not a valid message type.".format(_type))
			return

		await message.reply_text(str(matches_type(reply, _type)))


@Client.on_message(
	~filters.sticker
	& ~filters.via_bot
	& ~filters.forwarded
	& filters.me
	& filters.command(
		["tp", "typepurge", "stp", "selftypepurge"],
		prefixes=config["config"]["prefixes"],
	)
)
@log_errors
@public_log_errors
async def typePurge(client: Client, message: Message):
	"""{prefix}typepurge <i>(as reply to a message)</i> -t=type - Purges messages
Aliases: {prefix}tp

{prefix}selftypepurge <i>(as reply to a message)</i> -t=type - {prefix}yp but only your messages
Aliases: {prefix}stp
	"""
	reply: Message = message.reply_to_message
	if not reply:
		await message.delete()
		return

	owo = message.text.split(" ", 1)
	if len(owo) < 2:
		await self_destruct(message, "No type provided")
		return
	type = owo[1]
	if not is_valid_match_type(type):
		await self_destruct(message, "{} is not a valid message type.".format(type))
		return

	messages = {message.id}

	if not ("s" in message.command[0] and not reply.outgoing) and matches_type(reply, type):
		messages.add(reply.id)
	from_id, to_id = sorted((message.id, reply.id))

	async for i in client.get_chat_history(message.chat.id, offset_id=to_id):
		if not ("s" in message.command[0] and not i.outgoing) and matches_type(i, type):
			messages.add(i.id)
		if from_id >= i.id:
			break
	# convert to chunks and delete
	message_ids = list(messages)
	if len(message_ids) <= 100:
		await client.delete_messages(message.chat.id, message_ids)

	chunks = [message_ids[i:i + 100] for i in range(0, len(message_ids), 100)]
	for chunk in chunks:
		try:
			await client.delete_messages(message.chat.id, chunk)
		except:
			pass


__signature__ = "SHSIG-IF3sTTKjk1DzqALmaEHQMd5bid+fb9PWEFIrqYNPqaYvAAAAIPumu/b6lKH1KT57FP/Q6drfzJDQDC3RWaixIBRuO6R2AAAA"