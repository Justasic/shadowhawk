from pyrogram import filters
from pyrogram.enums import ChatType
from shadowhawk.utils.Logging import log_errors
from shadowhawk import slave, app_user_ids


@slave.on_message(filters.regex(r"^[\/!]start"))
@log_errors
async def start(_client, message):
	if message.chat.type != ChatType.PRIVATE:
		# Get ourselves
		username = (await slave.get_me()).username
		if username in message.text:
			if message.from_user.id in app_user_ids:
				await message.reply("Hiiii master! \N{heavy black heart}")
			else:
				await message.reply("owo who're you?")
	else:
		if message.from_user.id not in app_user_ids:
			await slave.send_message(
				message.chat.id, "Hewwo! I don't handle commands from strangers, sorry!"
			)
			return

		await slave.send_message(message.chat.id, "Under construction.")
