from shadowhawk.utils import get_entity
from pyrogram.enums import ChatMembersFilter

# This cache manager is meant to handle caching all admin IDs
# in a chat room, it will periodically expire them and renew
# the cache as-necessary. This is to avoid hundreds of lookups
# from the telegram API to know who is admin and who is not.
class AdminCacheManager:
	cache = {}

	def __init__(self):
		pass

	def Expire(self, chat_id):
		self.cache[chat_id].clear()

	def ExpireAll(self):
		self.cache.clear()

	# lookup a client without querying telegram
	def LookupNoQuery(self, chat_id, user_id):
		# Group Anonumous
		if user_id == 1087968824:
			return True
		if chat_id in self.cache:
			if user_id in self.cache[chat_id]:
				return True
		return False

	# lookup a user, if there is no cache, query telegram
	async def Lookup(self, client, chat_id, user_id):
		if chat_id in self.cache:
			return self.LookupNoQuery(chat_id, user_id)
		else:
			chat = await client.get_chat(chat_id)
			if not chat:
				return False

			admins = []
			async for member in client.get_chat_members(chat.id, filter=ChatMembersFilter.ADMINISTRATORS):
				admins.append(member.user.id)

			self.cache[chat.id] = admins
			return self.LookupNoQuery(chat_id, user_id)
