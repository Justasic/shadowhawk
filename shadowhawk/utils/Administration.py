from pyrogram.types import Message
from pyrogram.enums import ChatMembersFilter

async def is_admin(client, message, entity):
	chat_id = message
	if isinstance(chat_id, Message):
		chat_id = message.chat.id

	# Here lies the sanity checks
	admins = await client.get_chat_members(
		chat_id, filter=ChatMembersFilter.ADMINISTRATORS
	)
	admin_ids = [user.user.id for user in admins]
	return entity.id in admin_ids


async def CheckAdmin(client, message: Message):
	"""Check if we are an admin."""

	# Here lies the sanity checks
	admins = await client.get_chat_members(
		message.chat.id, filter=ChatMembersFilter.ADMINISTRATORS
	)
	admin_ids = [user.user.id for user in admins]
	me = await client.get_me()

	# If you are an admin
	if me.id in admin_ids:
		return True
	return False

	# ranks = ["administrator", "creator"]
	# me = await app.get_chat_member(chat_id=message.chat.id, user_id=message.from_user.id)
	# if me.status not in ranks:
	# 	return False
	# else:
	# 	if me.status != "administrator" or me.can_restrict_members:
	# 		return True
	# return False
