import re
from typing import Optional, Tuple

from pyrogram import Client
from pyrogram.types import Message, Chat, User
from pyrogram.enums import MessageEntityType
from pyrogram.errors.exceptions.bad_request_400 import UsernameNotOccupied

from shadowhawk.utils import get_entity
from shadowhawk import config
from shadowhawk.utils.Logging import log_chat

REGEX = re.compile(
	r'\s+--?(?P<flag>\w+)(?:\s*(?:=\s*|)(?:"(?P<string>(?:(?!(?<!\\)").)*)"|(?P<list>\[[^\[\]]+\])|(?P<arg>(?:\w+\-\w+|\-\d+|[\w\@\|\:\=]+)))|)'
)

def parse_command(message: str):
	owo = {}

	# Remove the prefix
	for prefix in config["config"]["prefixes"]:
		if message.startswith(prefix):
			message = message[len(prefix) :]

	# Parse the command structure
	for match in REGEX.finditer(message):
		flag = match.group("flag")
		string = match.group("string")
		arg = match.group("arg")
		items = match.group("list")
		if string:
			owo[flag] = string.replace('\\"', '"')
		elif items:
			# Parsing lists is a bit more complex
			# Sample input: [+CAN_ADD_MEMBERS, +CAN_USE_ANONYMOUS]
			owo[flag] = [x.strip() for x in items[1:-1].split(",")]
		else:
			owo[flag] = arg
	return owo

async def ResolveChatUser(owo: dict, client: Client, message: Message):
	# Attempt to resolve from the following:
	# - Mentions
	# - Replies
	# - Provided arguments

	user = None
	chat = message.chat

	# find keys, if this were C/C++ we could use if-assignment >:(
	key_chat = next((key for key, val in owo.items() if key in ["c", "chat", "group", "g", "channel"]), None)
	key_user = next((key for key, val in owo.items() if key in ["u", "user"]), None)

	# if it's a reply.
	if message.reply_to_message:
		# if it's a channel
		if message.reply_to_message.sender_chat:
			user = message.reply_to_message.sender_chat
		elif message.reply_to_message.from_user:
			user = message.reply_to_message.from_user

	# if there's other entities here to process
	if message.entities:
		for entity in message.entities:
			if entity.type == MessageEntityType.TEXT_MENTION:
				user = entity.user
			elif entity.type == MessageEntityType.MENTION:
				uwu = message.text[entity.offset : entity.offset + entity.length]
				try:
					ent, _ = await get_entity(client, uwu)
					user = ent
				except UsernameNotOccupied:
					# TODO: handle this maybe?
					pass
	if key_user:
		try:
			ent, _ = await get_entity(client, owo[key_user])
			user = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass
	if key_chat:
		try:
			ent, _ = await get_entity(client, owo[key_chat])
			chat = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass

	return user, chat


async def ParseInviteLink(
		msg: Message, client: Client
) -> Tuple[Optional[User], Optional[Chat], Optional[Message], dict]:

	if len(msg.text.split(" ")) <= 2:
		return None, None, None, {}
	link = msg.text.split(" ", 2)[1]
	owo = parse_command(msg.text.replace(" "+link, ""))
	parsed_chat = None
	msg_id = None
	user = None
	chat = None
	msg = None
	try:
		link = link.replace("telegram.me", "t.me").replace("telegram.dog", "t.me").replace("https://", "").replace("http://", "")
		if link.find("t.me/") == -1:
			return None, None, None, {}
		parsed = link.replace("t.me/c/", "").replace("t.me/", "").split("/")
		parsed_chat = int(f"-100{parsed[0]}") if str(parsed[0]).isnumeric() else parsed[0]
		msg_id = int(parsed[1])
	except Exception as err:
		await log_chat(f"error while resolving link:\n {err}")
	try:
		ent, _ = await get_entity(client, parsed_chat)
		chat = ent
	except UsernameNotOccupied:
		pass
	try:
		msg = await client.get_messages(chat.id, msg_id)
	except ValueError:
		pass
	try:
		ent, _ = await get_entity(client, msg.from_user)
		user = ent
	except UsernameNotOccupied:
		pass
	return user, chat, msg, owo

