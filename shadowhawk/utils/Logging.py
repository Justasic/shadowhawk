import asyncio
import functools
import traceback
import logging
import html
from asyncevents import on_event, emit
from pyrogram import StopPropagation, ContinuePropagation
from pyrogram.errors.exceptions.flood_420 import FloodWait
from shadowhawk.utils.FileUtils import make_file
from shadowhawk import config, slave, apps, statistics

log_ring = asyncio.Queue(maxsize=config["logging"]["regular_ring_size"])
spammy_log_ring = asyncio.Queue(maxsize=config["logging"]["spammy_ring_size"])

def log_errors(func):
	@functools.wraps(func)
	async def wrapper(client, *args):
		try:
			await func(client, *args)
		except (StopPropagation, ContinuePropagation):
			raise
		except Exception:
			tb = traceback.format_exc()
			is_vomit = len(tb) >= 4096
			try:
				if is_vomit:
					await slave.send_document(
						config["logging"]["regular"],
						make_file(tb.strip()),
						caption=f"Exception occured in <code>{func.__name__}</code>",
					)
				else:
					await slave.send_message(
						config["logging"]["regular"],
						f"Exception occured in <code>{func.__name__}\n\n{html.escape(tb)}</code>",
						disable_web_page_preview=True,
					)
			except Exception:
				logging.exception(
					"Failed to log exception for %s as slave", func.__name__
				)
				tb2 = traceback.format_exc()
				is_vomit_again = len(tb2) >= 4096
				for app in apps.values():
					try:
						if is_vomit:
							puke_msg = await app.send_document(
								config["logging"]["regular"],
								make_file(tb.strip()),
								caption=f"Exception occured in <code>{func.__name__}</code>",
							)
							if is_vomit_again:
								await puke_msg.reply_document(
									make_file(tb2.strip()),
									caption="Exception occured in the exception handler",
								)
							else:
								await puke_msg.reply(
									f"Exception occured in exception handler <code>{func.__name__}\n\n{html.escape(tb2)}</code>",
									disable_web_page_preview=True,
								)
						else:
							puke_msg = await app.send_message(
								config["logging"]["regular"],
								f"Exception occured in <code>{func.__name__}\n\n{html.escape(tb)}</code>",
								disable_web_page_preview=True,
							)
							if is_vomit_again:
								await puke_msg.reply_document(
									make_file(tb2.strip()),
									caption="Exception occured in the exception handler",
								)
							else:
								await puke_msg.reply(
									f"Exception occured in exception handler <code>{func.__name__}\n\n{html.escape(tb2)}</code>",
									disable_web_page_preview=True,
								)
					except Exception:
						logging.exception(
							"Failed to log exception for %s as app", func.__name__
						)
						tb = traceback.format_exc()
					else:
						break
				raise
			raise

	return wrapper


def public_log_errors(func):
	@functools.wraps(func)
	async def wrapper(client, message):
		try:
			await func(client, message)
		except (StopPropagation, ContinuePropagation):
			raise
		except Exception as ex:
			await message.reply_text(
				"<code>" + html.escape(f"{type(ex).__name__}: {str(ex)}") + "</code>",
				disable_web_page_preview=True,
			)
			raise

	return wrapper

async def log_chat(message: str, chat :str = "regular"):
	# Append it to the queue
	await emit("OnLogRingInsert", block=False, message=message, chat=chat)
	if chat == "regular":
		await log_ring.put((message, chat))
	else:
		try:
			spammy_log_ring.put_nowait((message, chat))
		except asyncio.QueueFull:
			pass

@on_event("OnStart")
async def _create_logging_task(_, EventName: str):
	async def _send_log_loop():
		flipflop = False
		while True:
			# send log messages to their chats
			# alternate logs
			flipflop = not flipflop
			msg = extra = ""
			chat = None
			sentlogs = 0
			while len(msg) < 4096:
				try:
					if flipflop:
						nmsg, chat = log_ring.get_nowait()
					else:
						nmsg, chat = spammy_log_ring.get_nowait()

					if msg:
						tstmsg = msg + "\n--------\n" + nmsg
						if len(tstmsg) > 4096:
							extra = nmsg
						else:
							msg = tstmsg
					else:
						msg = nmsg
					sentlogs += 1
				except asyncio.QueueEmpty:
					break

			while True and chat:
				try:
					await slave.send_message(
						config["logging"][chat], msg, disable_web_page_preview=True
					)
					if extra:
						await asyncio.sleep(config["logging"]["send_rate"])
						await slave.send_message(
							config["logging"][chat], extra, disable_web_page_preview=True
						)
						
					if "Logs Sent" in statistics:
						statistics["Logs Sent"] += sentlogs + 1 if extra else 0
					else:
						statistics["Logs Sent"] = sentlogs + 1 if extra else 0
				except FloodWait as ex:
					await asyncio.sleep(ex.value + 1)
				else:
					break
			
			await asyncio.sleep(config["logging"]["send_rate"])
	# Send our task to the async loop which logs to our log channels.
	asyncio.create_task(_send_log_loop())