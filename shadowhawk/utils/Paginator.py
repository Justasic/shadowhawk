import asyncio
import time
import traceback
from typing import List
from pyrogram import Client, filters, ContinuePropagation
from pyrogram.types import (
	InlineKeyboardMarkup,
	InlineKeyboardButton,
	InlineQueryResultArticle,
	InputTextMessageContent,
)
from shadowhawk import slave, app_user_ids
from shadowhawk.utils.Logging import log_errors


# Active pages we have
paginators = {}
# the lock for this page
page_lock = asyncio.Lock()


@Client.on_callback_query(filters.regex("^generic_nop$"))
@log_errors
async def generic_nop(client, callback_query):
	await callback_query.answer(cache_time=3600)


class Paginator:
	# Our current page
	page = 0

	# Scope is the scope of this content
	# content is what is to be paginated (it's an array)
	# Each page is an index into the content array
	def __init__(self, scope: str, title: str, content: List[str]):
		global paginators
		self.scope = scope
		self.content = content
		self.title = title
		# just make time an identifier
		self.identifier = int(time.time())
		# track our paginators
		paginators[self.identifier] = self

	def get_inline_start(self) -> str:
		return f"paginator_{self.scope}-{self.identifier}"

	def get_current_page(self) -> int:
		return self.page

	def get_total_pages(self) -> int:
		return len(self.content)

	def get_title(self) -> str:
		return self.title

	def next_page(self) -> str:
		self.page += 1
		if self.page > len(self.content):
			self.page = len(self.content)
		return self.get_page()

	def prev_page(self) -> str:
		self.page -= 1
		if self.page < 0:
			self.page = 0
		return self.get_page()

	def get_page(self) -> str:
		return self.content[self.page]


# This is sent when the client requests inline
@slave.on_inline_query(filters.regex(r"^paginator_(\w+)-(\d+)$"))
@log_errors
async def pagination_on_inline(client, query):
	global paginators
	try:
		scope = query.matches[0].group(1)
		identifier = int(query.matches[0].group(2))
		answers = []
		page = paginators[identifier]
		text = f"<b>{page.get_title()}</b>\n"
		text += page.get_page()

		for a, result in enumerate(page.content):
			full_snippet = text

			buttons = [
				InlineKeyboardButton(
					"Back", f"paginator_{scope}_prev={query.from_user.id}-{identifier}"
				),
				InlineKeyboardButton(
					f"{page.get_current_page() + 1}/{page.get_total_pages()}",
					"generic_nop",
				),
				InlineKeyboardButton(
					"Next", f"paginator_{scope}_next={query.from_user.id}-{identifier}"
				),
			]

			if not page.get_current_page():
				buttons.pop(0)
			if page.get_total_pages() == page.get_current_page() + 1:
				buttons.pop()

			answers.append(
				InlineQueryResultArticle(
					"Pagination " + scope,
					InputTextMessageContent(text, disable_web_page_preview=True),
					reply_markup=InlineKeyboardMarkup([buttons]),
					id=f"paginator{a}-{time.time()}",
					description=full_snippet,
				)
			)
		await query.answer(answers, is_personal=True)
	except Exception:
		traceback.print_exc()
	raise ContinuePropagation


@slave.on_callback_query(filters.regex(r"^paginator_(\w+)_(\w+)=(\d+)(?:-(\d+)|)$"))
@log_errors
async def paginator_on_callback(client, query):
	global page_lock, paginators
	try:
		scope = query.matches[0].group(1)
		subcmd = query.matches[0].group(2)
		target = int(query.matches[0].group(4))

		if query.from_user.id not in app_user_ids:
			await query.answer("...no", cache_time=3600, show_alert=True)
			raise ContinuePropagation

		async with page_lock:
			if target not in paginators:
				await query.answer(
					"This message is too old", cache_time=3600, show_alert=True
				)
				raise ContinuePropagation

			paginator: Paginator = paginators[target]

		opage = paginator.get_page()
		if subcmd == "next":
			page = paginator.next_page()
		elif subcmd == "prev":
			page = paginator.prev_page()

		if opage != page:
			text = f"<b>{paginator.get_title()}</b>\n"
			text += page
			buttons = [
				InlineKeyboardButton(
					"Back", f"paginator_{scope}_prev={query.from_user.id}-{target}"
				),
				InlineKeyboardButton(
					f"{paginator.get_current_page() + 1}/{paginator.get_total_pages()}",
					"generic_nop",
				),
				InlineKeyboardButton(
					"Next", f"paginator_{scope}_next={query.from_user.id}-{target}"
				),
			]
			if not paginator.get_current_page():
				buttons.pop(0)
			if paginator.get_total_pages() == paginator.get_current_page() + 1:
				buttons.pop()
			await query.edit_message_text(
				text,
				disable_web_page_preview=True,
				reply_markup=InlineKeyboardMarkup([buttons]),
			)
			async with page_lock:
				paginators[target] = paginator
		await query.answer()
	except Exception:
		traceback.print_exc()
	raise ContinuePropagation
