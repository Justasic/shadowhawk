import typing
import traceback
import logging
import sys
import gc
import ast
import base64
import types
from io import BytesIO
from pyrogram import Client
from pyrogram.handlers.handler import Handler
from pyrogram.raw.core.primitives import Bytes
from pathlib import Path
from importlib import import_module
from shadowhawk import config

log = logging.getLogger(__name__)
verify = False
triangulum = None
try:
	from fastecdsa.point import Point
	from fastecdsa import ecdsa
	verify = True
	triangulum = Point(35441751398416857496536618121866780581669428236947586928501161362012395270007,
                   111785166523856556894449646663410979586544367077887196612405512988573023988531)
except ImportError:
	log.warning("[PluginManager] FastECDSA not found, all plugins are considered unverified!")

class Plugin:
	# Name of the plugin
	name = str;
	# What handlers this plugin has
	handlers = typing.Dict[str, Handler]
	# The module itself (used for iterating, loading, unloading, etc.)
	module = typing.Union[None, types.ModuleType]
	# The path where the module was loaded from
	module_path = typing.Union[None, Path]
	# Clients which this plugin is registered with
	clients = typing.List[Client]
	# Plugin Cryptographic Signature
	signature = typing.Union[None, str]

	def __init__(self, name: str, module: types.ModuleType, module_path: Path, sig: typing.Union[str, None] = None) -> None:
		self.name = name
		self.module = module
		self.module_path = module_path
		self.handlers = dict()
		self.clients = list()
		self.signature = sig
		for name in vars(self.module).keys():
			try:
				for handler, group in getattr(self.module, name).handlers:
					self.handlers[(name, group)] = (handler, group)
			except Exception:
				pass

	def AddClient(self, client: Client) -> None:
		self.clients.append(client)
		for handler, group in self.handlers.values():
			client.add_handler(handler, group)

	def RemoveClient(self, client: Client) -> None:
		self.clients.remove(client)
		for handler, group in self.handlers.values():
			client.remove_handler(handler, group)

	def GetHelp(self) -> str:
		"""Get the formatted help for this plugin
		"""
		retstr = ""
		for name, _ in self.handlers.keys():
			try:
				retstr += getattr(self.module, name).__doc__ + "\n"
			except Exception:
				pass

		return retstr

	def GetHelpSection(self) -> str:
		"""Get the help "section" for this plugin
		"""
		sect = getattr(self.module, '__help_section__', None)
		return "Miscellaneous" if not sect else sect

class PluginManager:
	plugins = []

	def __init__(self, loaddir: Path) -> None:
		for path in sorted(Path(loaddir.replace(".", "/")).rglob("*.py")):
			self.LoadPlugin(path)

		# Support loading shared objects for nuitka
		for path in sorted(Path(str(loaddir).replace(".", "/")).rglob("*.so")):
			self.LoadPlugin(path)

		log.info(f"[PluginManager] Loaded a total of {len(self.plugins)} plugins!")

	def VerifyPlugin(self, modulepath: Path) -> typing.Union[None, str]:
		"""Cryptographically verify a plugin
		"""
		if verify:
			shsig = None
			with open(modulepath) as pyfile:
				tree = ast.parse(pyfile.read())
				for node in tree.body:
					if isinstance(node, ast.Assign) and isinstance(node.value, ast.Constant):
						for target in node.targets:
							if target.id == "__signature__":
								shsig = node.value.value
								tree.body.remove(node)
				if not shsig:
					return None

				data = ast.unparse(tree)
				shdat = BytesIO(base64.b64decode(shsig[6:]))
				r = int.from_bytes(Bytes.read(shdat), "big")
				s = int.from_bytes(Bytes.read(shdat), "big")
				if ecdsa.verify((r, s), data, triangulum):
					return shsig
		return None

	def LoadPlugin(self, path: Path) -> typing.Union[None, Plugin]:
		"""Load a plugin into ShadowHawk
		"""
		signature = self.VerifyPlugin(path)
		if not signature:
			if "pluginvalidation" in config['config'] and config['config']['pluginvalidation']['enabled']:
				log.warning(f"[PluginManager] [VERIFICATION] Module {path} is not a signed plugin, refusing to load!")
				return
			else:
				log.warning(f"[PluginManager] [VERIFICATION] Module {path} is not a signed plugin and plugin verification is disabled, please ensure you trust this plugin!")

		module_path = '.'.join(path.parent.parts + (path.stem,))
		try:
			module = import_module(module_path)

			if "__path__" in dir(module):
				log.warning(f"[PluginManager] Ignoring namespace \"{module_path}\"")
				return

			plugin = Plugin(path.stem, module, module_path, signature)
			self.plugins.append(plugin)

			log.info(f"[PluginManager]{' [VERIFIED]' if signature else ''} Module {plugin.module_path} loaded with {len(plugin.handlers)} handlers: " + ', '.join(name for name, _ in plugin.handlers))
			return plugin
		except Exception:
			traceback.print_exc()
			log.warning(f"[PluginManager] Failed to load {module_path}")
		return None

	def UnloadPlugin(self, plugin: Plugin) -> None:
		for client in plugin.clients:
			plugin.RemoveClient(client)
		
		self.plugins.remove(plugin)
		
		# XXX: It's not all that easy to remove a module which
		# has already been imported into Python. This may or
		# may not unload said plugin/module
		del sys.modules[plugin.module_path]
		plugin.module = None
		gc.collect()

	def RegisterClient(self, client: Client) -> None:
		log.info(f"[PluginManager] Registering new client: {client.name}")
		for plug in self.plugins:
			plug.AddClient(client)
		log.info(f"[PluginManager] Registration complete for {client.name}")

	def UnregisterClient(self, client: Client) -> None:
		log.info(f"[PluginManager] Removing client: {client.name}")
		for plug in self.plugins:
			plug.RemoveClient(client)
		log.info(f"[PluginManager] Removal for {client.name} complete!")

	def FindPlugin(self, name: typing.Union[str, types.ModuleType, Handler, typing.List[str]]) -> typing.Union[Plugin, None]:
		for plug in self.plugins:
			if isinstance(name, list):
				for n in name:
					if plug.name == n:
						return plug
					if plug.signature == n:
						return plug
					if plug.module_path and plug.module_path == n:
						return plug
					for hname, _ in plug.handlers:
						if n == hname:
							return plug
			if isinstance(name, str):
				if plug.name == name:
					return plug
				if plug.signature == name:
					return plug
				if plug.module_path and plug.module_path == name:
					return plug
				for hname, _ in plug.handlers:
					if name == hname:
						return plug
			elif isinstance(name, types.ModuleType):
				if plug.module == name:
					return plug
			elif isinstance(name, Handler):
				for _, handler in plug.handlers:
					if name == handler:
						return plug
		return None
