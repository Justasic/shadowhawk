import asyncio
import html
from pyrogram import Client
from pyrogram.types import User, Chat, Message, ChatMember
from pyrogram.enums import ChatType
from pyrogram.errors.exceptions.bad_request_400 import PeerIdInvalid, ChannelInvalid
from shadowhawk import slave, apps

def get_duration(duration: int):
	months = round(duration / 2629800)
	# there are generally 4 weeks in a month
	weeks = round((duration / 604800) % 4)
	days = round((duration / 86400) % 7)
	hours = round((duration / 3600) % 24)
	minutes = round((duration / 60) % 60)
	seconds = round((duration) % 60)

	if not months and not weeks and not days and not hours and not minutes:
		return f"{seconds} second{'s' if seconds != 1 else ''}"
	else:
		need_comma = False
		buffer = ""
		if months:
			buffer += f"{months} month{'s' if months != 1 else ''}"
			need_comma = True
		if weeks:
			buffer += f"{', ' if need_comma else ''}{weeks} week{'s' if weeks != 1 else ''}"
			need_comma = True
		if days:
			buffer += f"{', ' if need_comma else ''}{days} day{'s' if days != 1 else ''}"
			need_comma = True
		if hours:
			buffer += f"{', ' if need_comma else ''}{hours} hour{'s' if hours != 1 else ''}"
			need_comma = True
		if minutes:
			buffer += f"{', ' if need_comma else ''}{minutes} minute{'s' if minutes != 1 else ''}"
			need_comma = True
		if seconds:
			buffer += f"{', ' if need_comma else ''}{seconds} second{'s' if seconds != 1 else ''}"
		return buffer

def name_escape(name):
	if name:
		return "\u200E" + html.escape(name) + "\u200E"
	else:
		return None


async def build_name_flags(client, entity, ping=True, include_id=True):
	text = getattr(entity, "title", None)
	ent = entity
	if isinstance(entity, ChatMember):
		ent = entity.user

	if not text:
		text = ent.first_name
		if ent.last_name:
			text += f" {ent.last_name}"
	sexy_text = (
		"<code>[DELETED]</code>"
		if getattr(entity, "is_deleted", False)
		else name_escape(text or "Empty???")
	)
	if not getattr(ent, "is_deleted", False):
		if (
			ping
			and getattr(ent, "type", None)
			and ent.type in {ChatType.BOT, ChatType.PRIVATE}
			and text
		):
			sexy_text = f'<a href="tg://user?id={ent.id}">{sexy_text}</a>'
		elif ent.username:
			sexy_text = f'<a href="https://t.me/{ent.username}">{sexy_text}</a>'

	if not ping:
		sexy_text = sexy_text.replace("@", "@\u200B")
	if getattr(ent, "type", None) == "bot":
		sexy_text += " <code>[BOT]</code>"
	if getattr(ent, "is_verified", False):
		sexy_text += " <code>[VERIFIED]</code>"
	if getattr(ent, "is_support", False):
		sexy_text += " <code>[SUPPORT]</code>"
	if getattr(ent, "is_scam", False):
		sexy_text += " <code>[SCAM]</code>"
	if getattr(ent, "is_fake", False):
		sexy_text += " <code>[FAKE]</code>"

	if isinstance(entity, ChatMember):
		if getattr(entity, "is_anonymous", False):
			sexy_text += " <code>[ANON]</code>"
		if getattr(entity, "status", "") == "creator":
			sexy_text += " <code>[CREATOR]</code>"

	if include_id:
		sexy_text += f" [<code>{ent.id}</code>]"

	return sexy_text


async def get_entity(client, entity):
	entity_client = client
	if not isinstance(entity, Chat):
		try:
			entity = int(entity)
		except ValueError:
			pass
		except TypeError:
			if entity.id:
				entity = entity.id
		try:
			entity = await client.get_chat(entity)
		except (PeerIdInvalid, ChannelInvalid):
			for _, app in apps.items():
				if app != client:
					try:
						entity = await app.get_chat(entity)
					except (PeerIdInvalid, ChannelInvalid):
						pass
					else:
						entity_client = app
						break
			else:
				entity = await slave.get_chat(entity)
				entity_client = slave
	return entity, entity_client


async def get_user(client, entity):
	entity_client = client
	if not isinstance(entity, User):
		try:
			entity = int(entity)
		except ValueError:
			pass
		except TypeError:
			entity = entity.id
		try:
			entity = await client.get_users(entity)
		except PeerIdInvalid:
			for app in apps:
				if app != client:
					try:
						entity = await app.get_users(entity)
					except PeerIdInvalid:
						pass
					else:
						entity_client = app
						break
			else:
				entity = await slave.get_users(entity)
				entity_client = slave
	return entity, entity_client


async def get_app(id):
	for saved_id, app in apps.items():
		if saved_id == id:
			return app
	return None


def get_id_from_app(app: Client):
	return next((key for key, val in apps.items() if val == app), None)


async def get_chat_link(client, message):
	if not isinstance(message, Message):
		raise PeerIdInvalid

	if message.chat.username:
		return f"https://t.me/{message.chat.username}/{message.id}"
	elif getattr(message, "link", None):
		return message.link
	else:
		# Last resort.
		peer_id = (await client.resolve_peer(message.chat.id)).channel_id
		return f"https://t.me/c/{peer_id}/{message.id}"
	return ""


async def self_destruct(message, text):
	reply = None
	if message.from_user and message.from_user.is_self:
		await message.edit(text, disable_web_page_preview=True)
	else:
		reply = await message.reply(text, disable_web_page_preview=True)
	await asyncio.sleep(3)
	if reply:
		await reply.delete()

	# Try to delete our message or their message
	try:
		await message.delete()
	except Exception:
		pass
