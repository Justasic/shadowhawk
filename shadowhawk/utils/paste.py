from requests import post

from shadowhawk import config


def paste(text: str) -> (str, str):
	"""
	Paste text to pasty
	@param text: text to be pasted
	@return: pasty url and modification token
	"""
	data = {"content": text}
	resp = post(config['config']['paste_url'] + "api/v2/pastes", json=data)

	if resp.status_code in (200, 201):
		response = resp.json()
		final_url = config['config']['paste_url'] + response["id"]
		return final_url, response['modificationToken']
