from enum import Enum

from pyrogram.types import Message


class Types(str, Enum):
    a = "all"
    all = "all"
    me = "outgoing"
    outgoing = "outgoing"
    text = "text"
    sticker = "sticker"
    stk = "sticker"
    gif = "animation"
    animation = "animation"
    audio = "audio"
    file = "document"
    document = "document"
    doc = "document"
    service = "service"
    added = 'added_member'
    add = 'added_member'
    media = "media"
    bot = "is_bot"
    via_bot = "via_bot"
    inline = "via_bot"
    join = "new_chat_members"
    left = "left_chat_member"
    leave = "left_chat_member"
    new_title = "new_chat_title"
    new_photo = "new_chat_photo"
    del_photo = "delete_chat_photo"
    group_created = "group_chat_created"
    supergroup_created = "supergroup_chat_created"
    channel_created = "channel_chat_created"
    migrated_to = "migrate_to_chat_id"
    migrated_from = "migrate_from_chat_id"
    pinned = "pinned_message"


class NotAValidTypeError(Exception):
    pass


def matches_type(msg: Message, custom_type: str) -> bool:
    """
    Checks if the message matches the type.
    @param msg: Message to check
    @param custom_type: Type to check against (can be !type to check if the message is not of that type)
    @return: bool
    """
    custom_type = custom_type.lower().rstrip('s')
    inverse = False
    if custom_type.startswith('!'):
        custom_type = custom_type.lstrip('!')
        inverse = True
    does_match = False
    match custom_type:
        case "added", "add":
            if msg.service and msg.new_chat_members:
                for i in msg.new_chat_members:
                    if not i.is_bot:
                        does_match = True
        case "all", "a":
            does_match = True
        case "bot":
            does_match = msg.from_user.is_bot
        case _:
            if custom_type.lstrip("!") not in Types.__members__:
                raise NotAValidTypeError("{} is not a valid type".format(custom_type))
            does_match = hasattr(msg, Types[custom_type].value) and bool(getattr(msg, Types[custom_type].value))
    if inverse:
        return not does_match
    return does_match


def is_valid_match_type(string: str) -> bool:
    return string.rstrip('s').lstrip("!") in Types.__members__


def stringify_type(item):
    lines = []
    for member in item.__members__:
        lines.append(f"- {member}")
    return '\n'.join(lines)
